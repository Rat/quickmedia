#pragma once

namespace QuickMedia {
    float get_ui_scale();
    void show_virtual_keyboard();
    void hide_virtual_keyboard();
    bool is_touch_enabled();
}