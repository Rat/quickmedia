#pragma once

#include <SFML/Graphics/Image.hpp>

namespace QuickMedia {
    // See: https://github.com/SFML/SFML/issues/1711
    // TODO: Remove when above is fixed, or fix ourselves and include the fixed sfml version as a dependency,
    // or write our own image class (maybe even use stb like sfml does but no error reason printing)
    bool load_image_from_file(sf::Image &image, const std::string &filepath);
    bool load_image_from_memory(sf::Image &image, const void *data, size_t size);
}