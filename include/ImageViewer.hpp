#pragma once

#include "Path.hpp"
#include <string>
#include <vector>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Cursor.hpp>
#include <thread>

namespace sf {
    class RenderWindow;
}

namespace QuickMedia {
    class MangaImagesPage;

    enum class ImageStatus {
        WAITING,
        LOADING,
        FAILED_TO_LOAD,
        LOADED,
        APPLIED_TO_TEXTURE
    };

    struct ImageData {
        sf::Texture texture;
        sf::Sprite sprite;
        ImageStatus image_status;
        std::unique_ptr<sf::Image> image;
        bool visible_on_screen;
    };

    struct PageSize {
        sf::Vector2<double> size;
        bool loaded;
    };

    enum class ImageViewerAction {
        NONE,
        RETURN,
        SWITCH_TO_SINGLE_IMAGE_MODE
    };

    class ImageViewer {
    public:
        ImageViewer(sf::RenderWindow *window, MangaImagesPage *manga_images_page, const std::string &content_title, const std::string &chapter_title, int current_page, const Path &chapter_cache_dir);
        ~ImageViewer();
        ImageViewerAction draw();
        // Returns page as 1 indexed
        int get_focused_page() const;
        int get_num_pages() const { return num_pages; }
    private:
        void load_image_async(const Path &path, std::shared_ptr<ImageData> image_data);
        bool render_page(sf::RenderWindow &window, int page, double offset_y);
        sf::Vector2<double> get_page_size(int page);
    private:
        sf::RenderWindow *window;

        int current_page;
        int num_pages;
        int page_center;

        std::string content_title;
        std::string chapter_title;
        Path chapter_cache_dir;

        double scroll = 0.0;
        double scroll_speed = 0.0;
        double min_page_center_dist;
        int page_closest_to_center;
        int focused_page;
        int prev_focused_page = -1;

        sf::Font *font;
        sf::Clock frame_timer;
        sf::Text page_text;

        std::vector<std::shared_ptr<ImageData>> image_data;
        std::vector<PageSize> page_size;

        sf::Vector2<double> window_size;
        bool window_size_set = false;
        bool middle_mouse_scrolling = false;
        double autoscroll_start_y = 0.0;

        sf::Cursor default_cursor;
        sf::Cursor size_vertical_cursor;

        bool has_default_cursor;
        bool has_size_vertical_cursor;

        bool up_pressed = false;
        bool down_pressed = false;

        std::thread image_loader_thread;
        bool loading_image = false;
    };
}