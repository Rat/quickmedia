#pragma once

#include <string>
#include <vector>

namespace QuickMedia {
    struct Range {
        size_t start;
        size_t length;
    };

    void html_escape_sequences(std::string &str);
    void html_unescape_sequences(std::string &str);
    std::string url_param_encode(const std::string &param);
    std::vector<Range> extract_urls(const std::string &str);
    std::vector<std::string> ranges_get_strings(const std::string &str, const std::vector<Range> &ranges);
    void convert_utf8_to_utf32_ranges(const std::string &str, std::vector<Range> &ranges);
}