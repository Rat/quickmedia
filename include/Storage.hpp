#pragma once

#include "Path.hpp"
#include <functional>
#include <filesystem>
#include <json/value.h>
#include <rapidjson/fwd.h>

namespace QuickMedia {
    // Return false to stop the iterator
    using FileIteratorCallback = std::function<bool(const std::filesystem::path &filepath)>;

    enum class FileType {
        FILE_NOT_FOUND,
        REGULAR,
        DIRECTORY
    };

    Path get_home_dir();
    Path get_storage_dir();
    Path get_cache_dir();
    int get_cookies_filepath(Path &path, const std::string &plugin_name);
    int create_directory_recursive(const Path &path);
    FileType get_file_type(const Path &path);
    int file_get_content(const Path &path, std::string &result);
    int file_get_size(const Path &path, size_t *size);
    int file_overwrite(const Path &path, const std::string &data);
    int file_overwrite_atomic(const Path &path, const std::string &data);
    void for_files_in_dir(const Path &path, FileIteratorCallback callback);
    void for_files_in_dir_sort_last_modified(const Path &path, FileIteratorCallback callback);

    bool read_file_as_json(const Path &filepath, Json::Value &result);
    bool save_json_to_file_atomic(const Path &path, const Json::Value &json);
    bool save_json_to_file_atomic(const Path &path, const rapidjson::Value &json);

    bool is_program_executable_by_name(const char *name);
}