#pragma once

#include <SFML/System/Vector2.hpp>

// TODO: Check if size is 0 before dividing to prevent division by 0?

namespace QuickMedia {
    template<typename T>
    static T wrap_to_size_x(const T &size, const T &clamp_size) {
        T new_size;
        float size_ratio = (float)size.y / (float)size.x;
        new_size.x = clamp_size.x;
        new_size.y = new_size.x * size_ratio;
        return new_size;
    }

    template<typename T>
    static T wrap_to_size_y(const T &size, const T &clamp_size) {
        T new_size;
        float size_ratio = (float)size.x / (float)size.y;
        new_size.y = clamp_size.y;
        new_size.x = new_size.y * size_ratio;
        return new_size;
    }

    template<typename T>
    static T wrap_to_size(const T &size, const T &clamp_size) {
        T new_size;
        new_size = wrap_to_size_x(size, clamp_size);
        if(new_size.y > clamp_size.y)
            new_size = wrap_to_size_y(size, clamp_size);
        return new_size;
    }

    template<typename T>
    static T clamp_to_size(const T &size, const T &clamp_size) {
        T new_size = size;
        if(size.x > clamp_size.x || size.y > clamp_size.y)
            new_size = wrap_to_size(new_size, clamp_size);
        return new_size;
    }

    template<typename T>
    static sf::Vector2f get_ratio(const T &original_size, const T &new_size) {
        return sf::Vector2f((float)new_size.x / (float)original_size.x, (float)new_size.y / (float)original_size.y);
    }
}