#pragma once

#include "DownloadUtils.hpp"
#include <rapidjson/document.h>

namespace QuickMedia {
    const rapidjson::Value& GetMember(const rapidjson::Value &obj, const char *key);
    DownloadResult download_json(rapidjson::Document &result, const std::string &url, std::vector<CommandArg> additional_args, bool use_browser_useragent = true, std::string *err_msg = nullptr);
}