#pragma once

#include <deque>
#include <mutex>
#include <condition_variable>
#include <optional>

namespace QuickMedia {
    template <typename T>
    class MessageQueue {
    public:
        MessageQueue() : running(true) {

        }
    
        void push(T data) {
            std::unique_lock<std::mutex> lock(mutex);
            data_queue.push_back(std::move(data));
            cv.notify_one();
        }

        std::optional<T> pop_wait() {
            std::unique_lock<std::mutex> lock(mutex);
            if(!running)
                return std::nullopt;
            while(data_queue.empty() && running) cv.wait(lock);
            if(!running)
                return std::nullopt;
            T data = std::move(data_queue.front());
            data_queue.pop_front();
            return data;
        }

        std::optional<T> pop_if_available() {
            std::unique_lock<std::mutex> lock(mutex);
            if(data_queue.empty())
                return std::nullopt;
            T data = std::move(data_queue.front());
            data_queue.pop_front();
            return data;
        }

        void close() {
            std::unique_lock<std::mutex> lock(mutex);
            running = false;
            cv.notify_one();
        }

        void clear() {
            std::unique_lock<std::mutex> lock(mutex);
            data_queue.clear();
        }

        void restart() {
            std::unique_lock<std::mutex> lock(mutex);
            running = true;
        }
    private:
        std::deque<T> data_queue;
        std::mutex mutex;
        std::condition_variable cv;
        bool running;
    };
}