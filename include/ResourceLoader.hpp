#pragma once

namespace sf {
    class Font;
    class Texture;
}

namespace QuickMedia {
    void set_resource_loader_root_path(const char *resource_root);
}

namespace QuickMedia::FontLoader {
    enum class FontType {
        LATIN,
        LATIN_BOLD,
        CJK,
        EMOJI
    };

    // Note: not thread-safe
    sf::Font* get_font(FontType font_type);
}

namespace QuickMedia::TextureLoader {
    // Note: not thread-safe
    sf::Texture* get_texture(const char *filepath);
}