#pragma once

#include <string>

namespace QuickMedia {
    class Path {
    public:
        Path() = default;

        Path(const char *path) : data(path) {}
        Path(const std::string &path) : data(path) {}

        Path& join(const Path &other) {
            data += "/";
            data += other.data;
            return *this;
        }

        Path& append(const std::string &str) {
            data += str;
            return *this;
        }

        const char* filename() const {
            size_t index = data.rfind('/');
            if(index == std::string::npos)
                return data.c_str();
            return data.c_str() + index + 1;
        }

        // Returns empty string if no extension
        const char* ext() const {
            size_t slash_index = data.rfind('/');
            size_t index = data.rfind('.');
            if(index != std::string::npos && (slash_index == std::string::npos || index > slash_index))
                return data.c_str() + index;
            return "";
        }

        Path parent() {
            size_t slash_index = data.rfind('/');
            if(slash_index != std::string::npos || slash_index == 0)
                return Path(data.substr(0, slash_index));
            return Path("/");
        }

        std::string data;
    };
}