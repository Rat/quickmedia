#pragma once

#include "Text.hpp"
#include "AsyncImageLoader.hpp"
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "../external/RoundedRectangleShape.hpp"
#include <json/value.h>
#include <thread>
#include <future>

namespace sf {
    class RenderWindow;
    class Shader;
    class Texture;
    class Event;
}

namespace QuickMedia {
    class Program;

    enum class FetchStatus {
        NONE,
        QUEUED_LOADING,
        LOADING,
        FINISHED_LOADING,
        FAILED_TO_LOAD
    };

    enum class ThumbnailMaskType {
        NONE,
        CIRCLE
    };

    // TODO: Remove and create an Userdata class instead to replace the void* userdata in BodyItem
    class BodyItemExtra {
    public:
        virtual ~BodyItemExtra() = default;
    };

    struct Reaction {
        std::unique_ptr<Text> text;
        void *userdata = nullptr;
    };

    class BodyItem {
    public:
        BodyItem(std::string _title);
        BodyItem& operator=(BodyItem &other);

        static std::shared_ptr<BodyItem> create(std::string title) { return std::make_shared<BodyItem>(std::move(title)); }

        void set_title(std::string new_title) {
            if(title == new_title)
                return;
            title = std::move(new_title);
            dirty = true;
        }

        void set_description(std::string new_description) {
            if(description == new_description)
                return;
            description = std::move(new_description);
            dirty_description = true;
        }

        void set_author(std::string new_author) {
            if(author == new_author)
                return;
            author = std::move(new_author);
            dirty_author = true;
        }

        // |new_timestamp| is in milliseconds
        void set_timestamp(time_t new_timestamp) {
            if(new_timestamp == timestamp)
                return;
            timestamp = new_timestamp;
            dirty_timestamp = true;
        }

        void set_title_color(sf::Color new_color) {
            if(new_color == title_color)
                return;
            title_color = new_color;
            dirty = true;
        }

        void set_description_color(sf::Color new_color) {
            if(new_color == description_color)
                return;
            description_color = new_color;
            dirty_description = true;
        }

        void set_author_color(sf::Color new_color) {
            if(new_color == author_color)
                return;
            author_color = new_color;
            dirty_author = true;
        }

        void add_reaction(std::string text, void *userdata);

        // Returns true if reaction is found
        bool remove_reaction_by_userdata(void *userdata) {
            for(auto it = reactions.begin(); it != reactions.end(); ++it) {
                if(it->userdata == userdata) {
                    reactions.erase(it);
                    return true;
                }
            }
            return false;
        }

        const std::string& get_title() const { return title; }
        const std::string& get_description() const { return description; }
        const std::string& get_author() const { return author; }
        // In milliseconds
        time_t get_timestamp() const { return timestamp; }

        sf::Color get_title_color() const { return title_color; }
        sf::Color get_description_color() const { return description_color; }
        sf::Color get_author_color() const { return author_color; }

        // TODO: Use a list of strings instead, not all plugins need all of these fields
        std::string url;
        std::string thumbnail_url;
        std::string attached_content_url; // TODO: Remove and use |url| instead
        bool visible;
        bool dirty;
        bool dirty_description;
        bool dirty_author;
        bool dirty_timestamp;
        // TODO: Remove this and instead if |thumbnail_url| starts with file://, then its a local file
        bool thumbnail_is_local;
        std::unique_ptr<Text> title_text;
        std::unique_ptr<Text> description_text;
        std::unique_ptr<Text> author_text;
        std::unique_ptr<sf::Text> timestamp_text; // TODO: Remove
        // Used by image boards for example. The elements are indices to other body items
        std::vector<size_t> replies;
        std::string post_number;
        void *userdata; // Not managed, should be deallocated by whoever sets this
        double last_drawn_time;
        FetchStatus embedded_item_status = FetchStatus::NONE;
        // Important! Should refer to a new BodyItem, not one that already exists in the body.
        // TODO: Allow referring to an existing body item. This doesn't work properly at the moment because max width of text and line count calculation getting messed up
        // if an embedded item wraps but not the original body item.
        std::shared_ptr<BodyItem> embedded_item; // Used by matrix for example to display reply message body. Note: only the first level of embedded items is rendered (not recursive, this is done on purpose)
        ThumbnailMaskType thumbnail_mask_type = ThumbnailMaskType::NONE;
        sf::Vector2i thumbnail_size;
        std::vector<Reaction> reactions; // TODO: Move to a different body item type
        std::shared_ptr<BodyItemExtra> extra; // TODO: Remove
    private:
        // TODO: Clean up these strings when set in text, and get_title for example should return |title_text.getString()|
        // TODO: Use sf::String instead, removes the need to convert to utf32 every time the text is dirty (for example when resizing window)
        std::string title;
        std::string description;
        std::string author;
        time_t timestamp;
        sf::Color title_color;
        sf::Color author_color;
        sf::Color description_color;
    };

    using BodyItems = std::vector<std::shared_ptr<BodyItem>>;
    using BodyItemRenderCallback = std::function<void(BodyItem *body_item)>;
    // Return true to merge
    using BodyItemMergeHandler = std::function<bool(BodyItem *prev_item, BodyItem *this_item)>;

    enum class AttachSide {
        TOP,
        BOTTOM
    };

    class Body {
    public:
        Body(Program *program, sf::Texture &loading_icon_texture);
        ~Body();

        // Select previous page, ignoring invisible items. Returns true if the item was changed. This can be used to check if the top was hit when wrap_around is set to false
        bool select_previous_page();
        // Select next page, ignoring invisible items. Returns true if the item was changed. This can be used to check if the bottom was hit when wrap_around is set to false
        bool select_next_page();

        // Select previous item, ignoring invisible items. Returns true if the item was changed or if the item scrolled. This can be used to check if the top was hit when wrap_around is set to false
        bool select_previous_item(bool scroll_page_if_large_item = true, bool reset_select_scroll = true);

        // Select next item, ignoring invisible items. Returns true if the item was changed or if the item scrolled. This can be used to check if the bottom was hit when wrap_around is set to false
        bool select_next_item(bool scroll_page_if_large_item = true, bool reset_select_scroll = true);
        void set_selected_item(int item, bool reset_prev_selected_item = true);
        void reset_prev_selected_item();

        // Returns -1 if item can't be found
        int get_index_by_body_item(BodyItem *body_item);
        
        void select_first_item();
        void select_last_item();
        void clear_items();
        void prepend_items(BodyItems new_items);
        void append_items(BodyItems new_items);
        void insert_item_by_timestamp(std::shared_ptr<BodyItem> body_item);
        void insert_items_by_timestamps(BodyItems new_items);
        void clear_cache();
        void clear_text_cache();
        void clear_thumbnails();

        BodyItem* get_selected() const;
        std::shared_ptr<BodyItem> get_selected_shared();

        // Returns null if no visible items. This is the item we can see the end of
        BodyItem* get_last_fully_visible_item();

        void clamp_selection();

        // Returns true if the event was handled
        bool on_event(const sf::RenderWindow &window, const sf::Event &event);
        void draw(sf::RenderWindow &window, sf::Vector2f pos, sf::Vector2f size);
        void draw(sf::RenderWindow &window, sf::Vector2f pos, sf::Vector2f size, const Json::Value &content_progress);
        // |size| is the clip size, another outside this will be cut off.
        // Note: this should be called after |draw|, or thumbnails will be messed up. TODO: find a way to solve this issue in a clean way.
        // This happens because of |draw| sets thumbnails as unreferenced at the beginning and cleans them up at the end if they are not drawn in the same function call.
        // TODO: Right now drawing an item that also exists in the body will cause the text to update geometry every frame if the text is wrapping text and the items are drawn at different sizes,
        // because of Text::setMaxWidth
        void draw_item(sf::RenderWindow &window, BodyItem *item, sf::Vector2f pos, sf::Vector2f size, bool include_embedded_item = true, bool is_embedded = false);

        float get_item_height(BodyItem *item, float width, bool load_texture = true, bool include_embedded_item = true, bool merge_with_previous = false);
        float get_spacing_y() const;
        static bool string_find_case_insensitive(const std::string &str, const std::string &substr);

        // TODO: Make this actually fuzzy... Right now it's just a case insensitive string find.
        // This would require reordering the body.
        // TODO: Highlight the part of the text that matches the search.
        // TODO: Ignore dot, whitespace and special characters
        void filter_search_fuzzy(const std::string &text);
        void filter_search_fuzzy_item(const std::string &text, BodyItem *body_item);

        bool no_items_visible() const;

        int get_selected_item() const { return selected_item; }
        bool is_selected_item_last_visible_item() const;

        void set_page_scroll(float scroll);
        float get_page_scroll() const { return page_scroll; }
        // This is the item we can see the end of
        bool is_last_item_fully_visible() const { return last_item_fully_visible; }
        bool is_body_full_with_items() const { return items_cut_off; }
        int get_num_visible_items() const { return num_visible_items; };

        sf::Text progress_text;
        sf::Text replies_text;
        sf::Text embedded_item_load_text;
        BodyItems items;
        bool draw_thumbnails;
        bool wrap_around;
        // Set to {0, 0} to disable resizing
        sf::Vector2i thumbnail_max_size;
        sf::Color line_separator_color;
        BodyItemRenderCallback body_item_render_callback;
        BodyItemMergeHandler body_item_merge_handler;
        std::function<void(BodyItem*)> body_item_select_callback;
        sf::Shader *thumbnail_mask_shader;
        AttachSide attach_side = AttachSide::TOP;
        bool title_mark_urls = false;
    private:
        void draw_item(sf::RenderWindow &window, BodyItem *item, const sf::Vector2f &pos, const sf::Vector2f &size, const float item_height, const int item_index, const Json::Value &content_progress, bool include_embedded_item = true, bool merge_with_previous = false);
        void update_dirty_state(BodyItem *body_item, float width);
        void clear_body_item_cache(BodyItem *body_item);
        sf::Vector2i get_item_thumbnail_size(BodyItem *item) const;
        BodyItem* get_previous_visible_item(int start_index);
        BodyItem* get_next_visible_item(int start_index);
    private:
        Program *program;
        std::unordered_map<std::string, std::shared_ptr<ThumbnailData>> item_thumbnail_textures;
        int selected_item;
        int prev_selected_item;
        double page_scroll;
        // TODO: Use a loading gif or something similar instead, to properly indicate the image is loading. Which could also have text that says "failed to load image" when image loading failed.
        sf::RectangleShape image_fallback;
        sf::RectangleShape item_separator;
        sf::RoundedRectangleShape item_background;
        sf::Sprite image;
        sf::Sprite loading_icon;
        int num_visible_items;
        bool first_item_fully_visible;
        bool last_item_fully_visible;
        int first_fully_visible_item;
        int last_fully_visible_item;
        sf::Clock draw_timer;
        sf::Clock frame_timer;
        double elapsed_time_sec = 0.0;
        bool selected_line_top_visible = true;
        bool selected_line_bottom_visible = true;
        bool items_cut_off = false;
        float offset_to_top = 0.0f;
        float offset_to_bottom = 0.0f;
        int clamp_selected_item_to_body_count = 1;
        bool mouse_left_pressed = false;
        bool mouse_left_clicked = false;
        bool has_scrolled_with_input = false;
        sf::Vector2f mouse_click_pos;
        sf::Vector2f mouse_release_pos;
        double mouse_press_pixels_moved_abs;
        sf::Vector2f mouse_pos;
        sf::Vector2f prev_mouse_pos;
        sf::Vector2i mouse_pos_raw;
        sf::Vector2f mouse_scroll_accel;
        sf::Vector2f body_pos;
        sf::Vector2f body_size;
        float selected_item_height = 0.0f;
        float selected_scrolled = 0.0f;
        bool loaded_textures_changed = false;
        //float scroll_y = 0.0f;
    };
}