#pragma once

#include <memory>

namespace QuickMedia {
    class Body;
    class Page;
    class SearchBar;

    struct Tab {
        std::unique_ptr<Body> body;
        std::unique_ptr<Page> page; // Only null when current page has |is_single_page()| set to true
        std::unique_ptr<SearchBar> search_bar; // Nullable
    };
}