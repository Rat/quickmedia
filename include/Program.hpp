#ifndef QUICKMEDIA_PROGRAM_HPP
#define QUICKMEDIA_PROGRAM_HPP

#include <sys/types.h>
#include <thread>

typedef struct {
    pid_t pid;
    int read_fd;
} ReadProgram;

/* Return 0 if you want to continue reading. @data is null-terminated */
typedef int (*ProgramOutputCallback)(char *data, int size, void *userdata);

/*
    @args need to have at least 2 arguments. The first which is the program name
    and the last which is NULL, which indicates end of args
*/
int exec_program_pipe(const char **args, ReadProgram *read_program);

/*
    @args need to have at least 2 arguments. The first which is the program name
    and the last which is NULL, which indicates end of args.
    |buffer_size| has to be between 1 and 65536.
*/
int exec_program(const char **args, ProgramOutputCallback output_callback, void *userdata, int buffer_size = 4096);

// Return the exit status, or a negative value if waiting failed
int wait_program(pid_t process_id);

/* Returns 1 if the program quit and exited properly (non-0 exit codes also count as exiting properly) */
int wait_program_non_blocking(pid_t process_id, int *status);

/*
    @args need to have at least 2 arguments. The first which is the program name
    and the last which is NULL, which indicates end of args.
    @result_process_id should be set to NULL if you are not interested in the exit status of the child process
    and you want the child process to be cleaned up automatically when it dies.
*/
int exec_program_async(const char **args, pid_t *result_process_id);

void program_clear_current_thread();
void program_kill_in_thread(const std::thread::id &thread_id);

#endif /* QUICKMEDIA_PROGRAM_HPP */
