#pragma once

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include "../external/RoundedRectangleShape.hpp"
#include <functional>

namespace sf {
    class Font;
    class RenderWindow;
    class Event;
}

namespace QuickMedia {
    using TextUpdateCallback = std::function<void(const std::string &text)>;
    using TextSubmitCallback = std::function<void(const std::string &text)>;
    using TextBeginTypingCallback = std::function<void()>;
    using AutocompleteRequestCallback = std::function<void(const std::string &text)>;

    class SearchBar {
    public:
        SearchBar(sf::Texture *plugin_logo, const std::string &placeholder, bool input_masked = false);
        void draw(sf::RenderWindow &window, bool draw_shadow = true);
        void on_event(sf::Event &event);
        void update();
        void onWindowResize(const sf::Vector2f &window_size);
        void clear();
        void append_text(const std::string &text_to_add);
        bool is_cursor_at_start_of_line() const;
        void set_to_autocomplete();
        void set_autocomplete_text(const std::string &text);
        void set_vertical_position(float vertical_pos);
        void set_background_color(sf::Color color);

        float getBottom() const;
        float getBottomWithoutShadow() const;

        std::string get_text() const;

        TextUpdateCallback onTextUpdateCallback;
        TextSubmitCallback onTextSubmitCallback;
        TextBeginTypingCallback onTextBeginTypingCallback;
        AutocompleteRequestCallback onAutocompleteRequestCallback;
        int text_autosearch_delay;
        int autocomplete_search_delay;
        bool caret_visible;
    private:
        void onTextEntered(sf::Uint32 codepoint);
        void clear_autocomplete_if_text_not_substring();
        void clear_autocomplete_if_last_char_not_substr();
    private:
        sf::Text text;
        sf::Text autocomplete_text;
        sf::RoundedRectangleShape background;
        sf::RectangleShape background_shadow;
        sf::RectangleShape shade;
        sf::RectangleShape caret;
        sf::Sprite plugin_logo_sprite;
        std::string placeholder_str;
        bool show_placeholder;
        bool updated_search;
        bool updated_autocomplete;
        bool draw_logo;
        bool needs_update;
        bool input_masked;
        bool typing;
        bool backspace_pressed;
        bool mouse_left_inside;
        float vertical_pos;
        sf::Clock time_since_search_update;
    };
}