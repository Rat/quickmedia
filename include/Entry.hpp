#pragma once

#include "Text.hpp"
#include "../external/RoundedRectangleShape.hpp"
#include <SFML/Graphics/Text.hpp>
#include <functional>

namespace sf {
    class Font;
    class Event;
    class RenderWindow;
}

namespace QuickMedia {
    // Return true to clear the text
    using OnEntrySubmit = std::function<bool(std::string text)>;

    class Entry {
    public:
        Entry(const std::string &placeholder_text);
        void process_event(sf::Event &event);
        void draw(sf::RenderWindow &window);

        void set_editable(bool editable);
        void set_text(std::string text);
        void set_position(const sf::Vector2f &pos);
        void set_max_width(float width);
        void move_caret_to_end();
        void append_text(std::string str);

        float get_height();

        OnEntrySubmit on_submit_callback;
        bool draw_background;
    private:
        Text text;
        float width;
        sf::RoundedRectangleShape background;
        sf::Text placeholder;
        bool mouse_left_inside;
    };
}