#pragma once

#include <stddef.h>
#include <optional>
#include <string>

namespace QuickMedia {
    struct Dimensions {
        int width;
        int height;
    };

    enum class ContentType {
        UNKNOWN,
        VIDEO_AVI,
        VIDEO_MP4,
        VIDEO_WEBM,
        VIDEO_FLV,
        AUDIO_BASIC,
        AUDIO_AIFF,
        AUDIO_MPEG,
        AUDIO_MIDI,
        AUDIO_WAVE,
        AUDIO_FLAC,
        AUDIO_OPUS,
        IMAGE_JPEG,
        IMAGE_PNG,
        IMAGE_GIF,
        IMAGE_BMP,
        IMAGE_WEBP
    };

    bool is_content_type_video(ContentType content_type);
    bool is_content_type_audio(ContentType content_type);
    bool is_content_type_image(ContentType content_type);
    const char* content_type_to_string(ContentType content_type);
    bool is_image_ext(const char *ext);
    bool is_video_ext(const char *ext);

    // Set |width| or |height| to 0 to disable scaling.
    // This function is async.
    bool video_get_first_frame(const char *filepath, const char *destination_path, int width = 0, int height = 0);
    
    class FileAnalyzer {
    public:
        FileAnalyzer();
        bool load_file(const char *filepath, bool load_file_metadata = true);
        ContentType get_content_type() const;
        size_t get_file_size() const;
        std::optional<Dimensions> get_dimensions() const;
        std::optional<double> get_duration_seconds() const;
    private:
        FileAnalyzer(FileAnalyzer&) = delete;
        FileAnalyzer& operator=(FileAnalyzer&) = delete;
    private:
        ContentType content_type;
        size_t file_size;
        std::optional<Dimensions> dimensions;
        std::optional<double> duration_seconds;
        bool loaded;
    };
}