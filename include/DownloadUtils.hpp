#pragma once

#include "Path.hpp"
#include <string>
#include <vector>
#include <functional>
#include <rapidjson/fwd.h>

namespace QuickMedia {
    enum class DownloadResult {
        OK,
        ERR,
        NET_ERR
    };

    struct CommandArg {
        std::string option;
        std::string value;
    };

    // Return true the return DownloadResult::OK for the download, which also saves the result in cache if |download_to_string_cache| is used
    using DownloadErrorHandler = std::function<bool(std::string&)>;

    DownloadResult download_to_string(const std::string &url, std::string &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent = false, bool fail_on_error = true);
    DownloadResult download_to_string_cache(const std::string &url, std::string &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent = false, DownloadErrorHandler error_handler = nullptr, Path cache_path = "");
    DownloadResult download_to_file(const std::string &url, const std::string &destination_filepath, const std::vector<CommandArg> &additional_args, bool use_browser_useragent = false);
    DownloadResult download_to_json(const std::string &url, rapidjson::Document &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent = false, bool fail_on_error = true);
}