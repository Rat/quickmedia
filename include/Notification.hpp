#pragma once

#include <string>

namespace QuickMedia {
    enum class Urgency {
        LOW,
        NORMAL,
        CRITICAL
    };

    const char* urgency_string(Urgency urgency);
    void show_notification(const std::string &title, const std::string &description, Urgency urgency = Urgency::NORMAL);
}