#pragma once

#include "Path.hpp"

namespace QuickMedia {
    enum class ImageType {
        JPG,
        PNG,
        GIF
    };

    // Works with jpg, png and gif files. |image_type| can be NULL
    bool image_get_resolution(const Path &path, int *width, int *height, ImageType *image_type = nullptr);
    bool is_image_ext(const char *ext);
}