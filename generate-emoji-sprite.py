#!/usr/bin/env python3

from gi.repository import Gtk, Gdk, Pango, PangoCairo
import cairo

surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 1024, 1512)
context = cairo.Context(surface)
pc = PangoCairo.create_context(context)
layout = PangoCairo.create_layout(context)
layout.set_font_description(Pango.FontDescription('NotoSans Emoji Normal 12'))
layout.set_width(1024*Pango.SCALE)
layout.set_wrap(Pango.WrapMode.CHAR)

emoji_start = 0x1F000
emoji_end   = 0x1FB6F
emoji_characters = []
i = emoji_start
while i <= emoji_end:
    emoji_characters.append(chr(i))
    i += 1

layout.set_text("".join(emoji_characters), -1)
PangoCairo.show_layout(context, layout)
surface.write_to_png('images/emoji.png')

with open("generated/Emoji.hpp", "w") as header_file:
    header_file.write("""#pragma once

#include <stdint.h>

// This file was automatically generated with generate-emoji-sprite.py, do not edit manually!

namespace QuickMedia {
    struct EmojiRectangle {
        int x, y;
        int width, height;
    };

    bool codepoint_is_emoji(uint32_t codepoint);
    EmojiRectangle emoji_get_extents(uint32_t codepoint);
}""")

with open("generated/Emoji.cpp", "w") as source_file:
    source_file.write("""#include "Emoji.hpp"
#include <assert.h>

// This file was automatically generated with generate-emoji-sprite.py, do not edit manually!

namespace QuickMedia {
    static EmojiRectangle emoji_extents[%s] = {
""" % len(emoji_characters))

    i = 0
    index = 0
    for e in emoji_characters:
        emoji_pos = layout.index_to_pos(index)
        source_file.write("        {%s, %s, %s, %s}%s\n" % (int(emoji_pos.x/Pango.SCALE), int(emoji_pos.y/Pango.SCALE), int(emoji_pos.width/Pango.SCALE), int(emoji_pos.height/Pango.SCALE), "," if i < len(emoji_characters) - 1 else ""))
        i += 1
        index += 4

    source_file.write(
"""    };

    bool codepoint_is_emoji(uint32_t codepoint) {
        return codepoint >= 0x1F000 && codepoint <= 0x1FB6F;
    }

    EmojiRectangle emoji_get_extents(uint32_t codepoint) {
        assert(codepoint_is_emoji(codepoint));
        return emoji_extents[codepoint - 0x1F000];
    }
}
""")