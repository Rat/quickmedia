uniform sampler2D texture;

void main() {
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);
    vec2 pixelOffset = gl_TexCoord[0].xy - vec2(0.5, 0.5);
    float dist = sqrt(dot(pixelOffset, pixelOffset));
    dist = smoothstep(0.47, 0.5, dist);
    gl_FragColor = gl_Color * pixel * vec4(1.0, 1.0, 1.0, 1.0 - dist);
}
