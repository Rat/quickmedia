#include <HtmlParser.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char* file_get_content(const char *path, long *filesize) {
    FILE *file = fopen(path, "rb");
    if(!file) {
        perror(path);
        return NULL;
    }

    fseek(file, 0, SEEK_END);
    *filesize = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *data = malloc(*filesize);
    fread(data, 1, *filesize, file);
    fclose(file);
    return data;
}

static void html_parse_callback(HtmlParser *html_parser, HtmlParseType parse_type, void *userdata_any) {
    switch(parse_type) {
        case HTML_PARSE_TAG_START:
            printf("tag start: %.*s\n", html_parser->tag_name.size, html_parser->tag_name.data);
            break;
        case HTML_PARSE_TAG_END:
            printf("tag end: %.*s\n", html_parser->tag_name.size, html_parser->tag_name.data);
            break;
    }
}

int main() {
    long filesize;
    char *file_data = file_get_content("tests/github.html", &filesize);
    if(!file_data) {
        fprintf(stderr, "Failed to read from file: tests/github.html\n");
        return 1;
    }

    HtmlParser html_parser;
    html_parser_init(&html_parser, file_data, filesize, html_parse_callback, NULL);
    html_parser_parse(&html_parser);
    html_parser_deinit(&html_parser);
    free(file_data);
    return 0;
}
