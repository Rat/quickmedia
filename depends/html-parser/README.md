A small html parser written in C. The parser fixes broken html (missing end tags). The parser doesn't perform any dynamic allocations (heap) and neither copies any text, and only outputs the parsing result to a callback function rather than a dom tree.
# TODO
Unescape html sequences in text and attribute values
