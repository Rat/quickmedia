#include "../include/quickmedia/NodeSearch.h"
#include <stdlib.h>

void quickmedia_node_search_param_init(QuickMediaNodeSearchParam *self) {
    self->name = NULL;
    self->value = NULL;
    self->defined = 0;
}

static void quickmedia_node_search_param_deinit(QuickMediaNodeSearchParam *self) {
    free(self->name);
    free(self->value);
    self->name = NULL;
    self->value = NULL;
}

void quickmedia_node_search_init(QuickMediaNodeSearch *self) {
    self->name = NULL;
    self->recursive = 0;
    quickmedia_node_search_param_init(&self->param);
    self->child = NULL;
}

void quickmedia_node_search_deinit(QuickMediaNodeSearch *self) {
    free(self->name);
    self->name = NULL;
    quickmedia_node_search_param_deinit(&self->param);
    
    if(self->child) {
        quickmedia_node_search_deinit(self->child);
        free(self->child);
        self->child = NULL;
    }
}
