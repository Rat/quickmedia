#include <stdio.h>
#include "../include/quickmedia/HtmlSearch.h"
#include <assert.h>
#include <stdlib.h>

static char* get_file_content(const char *filepath) {
    FILE *file = fopen(filepath, "rb");
    assert(file);

    fseek(file, 0, SEEK_END);
    size_t filesize = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *buffer = malloc(filesize + 1);
    buffer[filesize] = '\0';
    fread(buffer, 1, filesize, file);

    return buffer;
}

static void result_callback(QuickMediaHtmlNode *node, void *userdata) {
    const char *href = quickmedia_html_node_get_attribute_value(node, "href");
    const char *text = quickmedia_html_node_get_text(node);
    printf("a href: %s, node value: %s\n", href, text);
}

int main(int argc, char **argv) {
    char *file_content = get_file_content("test_files/test.html");
    QuickMediaHtmlSearch html_search;

    int result = quickmedia_html_search_init(&html_search, file_content);
    if(result != 0)
        goto cleanup;
    result = quickmedia_html_find_nodes_xpath(&html_search, "//h3[class=\"story_name\"]//a", result_callback, NULL);
    /* Test that the object can be reused without reloading html doc */
    result = quickmedia_html_find_nodes_xpath(&html_search, "//h3[class=\"story_name\"]//a", result_callback, NULL);

    cleanup:
    quickmedia_html_search_deinit(&html_search);
    free(file_content);
    return result;
}
