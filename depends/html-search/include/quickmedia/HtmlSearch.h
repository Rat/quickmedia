#ifndef QUICKMEDIA_HTML_SEARCH_H
#define QUICKMEDIA_HTML_SEARCH_H

#include "NodeSearch.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    const void *doc;
    const void *node;
    void *text;
} QuickMediaHtmlNode;

typedef struct {
    const void *doc;
} QuickMediaHtmlSearch;

/*
    Returns NULL if attribute doesn't exist or if it doesn't have any value.
    The result is only valid within the callback function scope.
*/
const char* quickmedia_html_node_get_attribute_value(QuickMediaHtmlNode *self, const char *attribute_name);

/*
    Returns NULL if the node doesn't have any text.
    The result is only valid within the callback function scope.
*/
const char* quickmedia_html_node_get_text(QuickMediaHtmlNode *self);

/* @node is only valid within the callback function scope */
typedef void (*QuickMediaHtmlSearchResultCallback)(QuickMediaHtmlNode *node, void *userdata);

int quickmedia_html_search_init(QuickMediaHtmlSearch *self, const char *html_source);
void quickmedia_html_search_deinit(QuickMediaHtmlSearch *self);

/* Non-standard xpath. Doesn't use '@' symbol for accessing properties */
int quickmedia_html_find_nodes_xpath(QuickMediaHtmlSearch *self, const char *xpath, QuickMediaHtmlSearchResultCallback result_callback, void *userdata);

#ifdef __cplusplus
}
#endif

#endif
