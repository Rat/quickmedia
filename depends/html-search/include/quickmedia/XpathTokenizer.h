#ifndef QUICKMEDIA_XPATH_TOKENIZER_H
#define QUICKMEDIA_XPATH_TOKENIZER_H

#include "NodeSearch.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    const char *code;
    union {
        QuickMediaStringView string;
        QuickMediaStringView identifier;
    };
} QuickMediaXpathTokenizer;

typedef enum {
    QUICKMEDIA_XPATH_TOKEN_INVALID,
    QUICKMEDIA_XPATH_TOKEN_END_OF_FILE,
    QUICKMEDIA_XPATH_TOKEN_CHILD,
    QUICKMEDIA_XPATH_TOKEN_CHILD_RECURSIVE,
    QUICKMEDIA_XPATH_TOKEN_IDENTIFIER,
    QUICKMEDIA_XPATH_TOKEN_STRING,
    QUICKMEDIA_XPATH_TOKEN_OPEN_BRACKET,
    QUICKMEDIA_XPATH_TOKEN_CLOSING_BRACKET,
    QUICKMEDIA_XPATH_TOKEN_EQUAL
} QuickMediaXpathToken;

void quickmedia_xpath_tokenizer_init(QuickMediaXpathTokenizer *self, const char *xpath);
QuickMediaXpathToken quickmedia_xpath_tokenizer_next(QuickMediaXpathTokenizer *self);
int quickmedia_xpath_tokenizer_next_if(QuickMediaXpathTokenizer *self, QuickMediaXpathToken token);
char* quickmedia_xpath_tokenizer_copy_identifier(QuickMediaXpathTokenizer *self);
char* quickmedia_xpath_tokenizer_copy_string(QuickMediaXpathTokenizer *self);

#ifdef __cplusplus
}
#endif

#endif
