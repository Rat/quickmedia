#ifndef QUICKMEDIA_XPATH_PARSER_H
#define QUICKMEDIA_XPATH_PARSER_H

#include "NodeSearch.h"

#ifdef __cplusplus
extern "C" {
#endif

int quickmedia_parse_xpath(const char *xpath, QuickMediaNodeSearch *result);

#ifdef __cplusplus
}
#endif

#endif
