#ifndef QUICKMEDIA_NODE_SEARCH_H
#define QUICKMEDIA_NODE_SEARCH_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    char *name;
    char *value;
    int defined;
} QuickMediaNodeSearchParam;

typedef struct QuickMediaNodeSearch QuickMediaNodeSearch;

struct QuickMediaNodeSearch {
    char *name; /* optional */
    int recursive;
    QuickMediaNodeSearchParam param; /* optional */
    QuickMediaNodeSearch *child; /* optional */
};

typedef struct {
    const char *data;
    unsigned long long size;
} QuickMediaStringView;

void quickmedia_node_search_param_init(QuickMediaNodeSearchParam *self);
void quickmedia_node_search_init(QuickMediaNodeSearch *self);
void quickmedia_node_search_deinit(QuickMediaNodeSearch *self);

#ifdef __cplusplus
}
#endif

#endif
