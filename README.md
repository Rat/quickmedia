# QuickMedia
A dmenu-inspired native client for web services.
Currently supported web services: `youtube`, `spotify (podcasts)`, `soundcloud`, `nyaa.si`, `manganelo`, `mangatown`, `mangadex`, `4chan`, `matrix` and _others_.\
**Note:** file-manager is early in progress.\
Config data, including manga progress is stored under `$HOME/.config/quickmedia`.\
Cache is stored under `$HOME/.cache/quickmedia`.
## Usage
```
usage: quickmedia <plugin> [--use-system-mpv-config] [--dir <directory>] [-e <window>]
OPTIONS:
  plugin                      The plugin to use. Should be either launcher, 4chan, manganelo, mangatown, mangadex, youtube, spotify, soundcloud, nyaa.si, matrix, file-manager or stdin
  --no-video                  Only play audio when playing a video. Disabled by default
  --use-system-mpv-config     Use system mpv config instead of no config. Disabled by default
  --upscale-images            Upscale low-resolution manga pages using waifu2x-ncnn-vulkan. Disabled by default
  --upscale-images-always     Upscale manga pages using waifu2x-ncnn-vulkan, no matter what the original image resolution is. Disabled by default
  --dir <directory>           Set the start directory when using file-manager
  -e <window>                 Embed QuickMedia into another window
EXAMPLES:
  quickmedia launcher
  quickmedia --upscale-images-always manganelo
  echo -e "hello\nworld" | quickmedia stdin
  tabbed quickmedia launcher -e
```
## Installation
If you are running arch linux then you can install QuickMedia from aur (https://aur.archlinux.org/packages/quickmedia-git/), otherwise you will need to use [sibs](https://git.dec05eba.com/sibs/) to build QuickMedia manually.
## Controls
Press `Arrow up` / `Arrow down` or `Ctrl+K` / `Ctrl+J` to navigate the menu and also to scroll to the previous/next image when viewing manga in scroll mode. Alternatively you can use the mouse scroll to scroll to the previous/next manga in scroll mode.\
Press `Arrow left` / `Arrow right` or `Ctrl+H` / `Ctrl+L` to switch tab.\
Press `Page up` to scroll up and entire page or `Page down` to scroll down an entire page.\
Press `Home` to scroll to the top or `End` to scroll to the bottom.\
Press `Enter` (aka `Return`) to select the item.\
Press `ESC` to go back to the previous menu.\
Press `ESC` or `Backspace` to close the video.\
Press `Ctrl + F` to switch between window mode and fullscreen mode when watching a video.\
Press `Space` to pause/unpause a video.\
Press `Ctrl + R` to show video comments, related videos or video channel when watching a video (if supported).\
Press `Ctrl + T` when hovering over a manga chapter to start tracking manga after that chapter. This only works if AutoMedia is installed.
Press `Backspace` to return to the preview item when reading replies in image board threads.\
Press `R` to paste the post number of the selected post into the post field (image boards).\
Press `I` to begin writing a post to a thread (image boards), press `ESC` to cancel.\
Press `1 to 9` or `Numpad 1 to 9` to select google captcha image when posting a comment on 4chan.\
Press `P` to preview the 4chan image of the selected row in full screen view, press `ESC` or `Backspace` to go back.\
Press `I` to switch between single image and scroll image view mode when reading manga.\
Press `F` to fit image to window size when reading manga. Press `F` again to show original image size.\
Press `Middle mouse button` to "autoscroll" in scrolling image view mode.\
Press `Tab` to switch between username/password field in login panel.\
Press `Ctrl + C` to copy the url of the currently playing video to the clipboard (with timestamp).\
Press `Ctrl + V` to paste the content of your clipboard into the search bar.\
Press `Enter` to view image/video attached to matrix message, or to view the url in the message in quickmedia (youtube) or in the browser.\
Press `I` to begin writing a message in a matrix room, press `ESC` to cancel.\
Press `R` to reply to a message on matrix, press `ESC` to cancel.\
Press `E` to edit a message on matrix, press `ESC` to cancel. Currently only works for your own messages.\
Press `Ctrl + D` to delete a message on matrix. Currently deleting a message only deletes the event, so if you delete an edit then the original message wont be deleted.\
Press `Ctrl + C` to copy the message of the selected item in matrix to the clipboard.\
Press `Ctrl + V` to upload media to room in matrix if the clipboard contains a valid absolute filepath.\
Press `Ctrl+Alt+Arrow up` / `Ctrl+Alt+Arrow down` or `Ctrl+Alt+K` / `Ctrl+Alt+J` to view the room above/below the selected room in matrix.

In matrix you can select a message with enter to open the url in the message (or if there are multiple urls then a menu will appear for selecting which to open).
## Matrix commands
`/upload` to upload an image.\
`/logout` to logout.\
`/leave` to leave the current room.\
`/me [text]` to send a message of type "m.emote".\
`/react [text]` to react to the selected message.
## Mangadex
To search for manga with mangadex, you need to be logged into mangadex in your browser and copy the `mangadex_rememberme_token` cookie from developer tools
and store it in `$HOME/.config/quickmedia/credentials/mangadex.json` under the key `rememberme_token`. Here is an example what the file should look like:
```
{
    "rememberme_token": "21s9d3f7ad224a131239Dsfaf033029d2e390dAsfd3ldadb3a39dk43jfldj35das"
}
```
## Environment variables
Set `QM_PHONE_FACTOR=1` to disable the room list side panel in matrix.
## UI scaling
Either set the `GDK_SCALE` environment variable or add `Xft.dpi` to `$HOME/.Xresources` (`xrdb` which is part of the `xorg-xrdb` package needs to be installed).\
For example a value of 96 for the `Xft.dpi` means 1.0 scaling and 144 (96*1.5) means 1.5 scaling.\
Note that at the moment, cached images will not be scaled with the dpi. Images do also not scale above their original size.
## Tabs
[tabbed](https://tools.suckless.org/tabbed/) can be used to put quickmedia windows into tabs. After installing `tabbed`, run `tabbed quickmedia launcher -e`.
# Dependencies
## Compile
See project.conf \[dependencies].
## Runtime
### Required
`curl` is required for network requests.\
`imagemagick` is required for thumbnails.\
`noto-fonts` and `noto-fonts-cjk` is required for latin and japanese characters.
### Optional
`mpv` needs to be installed to play videos.\
`youtube-dl` needs to be installed to play youtube music/video or spotify podcasts.\
`notify-send` needs to be installed to show notifications (on Linux and other systems that uses d-bus notification system).\
[automedia](https://git.dec05eba.com/AutoMedia/) needs to be installed when tracking manga with `Ctrl + T`.\
`waifu2x-ncnn-vulkan` needs to be installed when using the `--upscale-images` or `--upscale-images-always` option.\
`xdg-utils` which provides `xdg-open` needs to be installed when downloading torrents with `nyaa.si` plugin.\
`ffmpeg (and ffprobe which is included in ffmpeg)` needs to be installed to upload videos with thumbnails on matrix.
## License
QuickMedia is free software licensed under GPL 3.0, see LICENSE for more details. `images/emoji.png` uses Noto Color Emoji, which is licensed under the Apache license, see: https://github.com/googlefonts/noto-emoji.
# Screenshots
## Youtube search
![](https://www.dec05eba.com/images/youtube.jpg)
## Youtube video
![](https://www.dec05eba.com/images/youtube-video.jpg)
## Youtube comments
![](https://www.dec05eba.com/images/youtube-comments.png)
## Manganelo search
![](https://www.dec05eba.com/images/manganelo.jpg)
## Manganelo chapters
![](https://www.dec05eba.com/images/manganelo-chapters.jpg)
## Manganelo page
![](https://www.dec05eba.com/images/manganelo-page.jpg)
## 4chan thread
![](https://www.dec05eba.com/images/4chan.jpg)
## Matrix chat
![](https://www.dec05eba.com/images/matrix.jpg)
