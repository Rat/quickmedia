#pragma once

#include <stdint.h>

// This file was automatically generated with generate-emoji-sprite.py, do not edit manually!

namespace QuickMedia {
    struct EmojiRectangle {
        int x, y;
        int width, height;
    };

    bool codepoint_is_emoji(uint32_t codepoint);
    EmojiRectangle emoji_get_extents(uint32_t codepoint);
}