#include "../include/Utils.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>

namespace QuickMedia {
    static float scale = 1.0f;
    static bool scale_set = false;
    static bool qm_enable_touch = false;
    static bool qm_enable_touch_set = false;

    static const int XFT_DPI_DEFAULT = 96;
    // Returns 96 on error
    static int xrdb_get_dpi() {
        int xft_dpi = XFT_DPI_DEFAULT;

        FILE *xrdb_query = popen("xrdb -query", "r");
        if(!xrdb_query)
            return xft_dpi;

        char line[512];
        while(fgets(line, sizeof(line), xrdb_query)) {
            int line_length = strlen(line);
            if(line_length > 0 && line[line_length - 1] == '\n') {
                line[line_length - 1] = '\0';
                line_length--;
            }

            if(line_length > 8 && memcmp(line, "Xft.dpi:", 8) == 0) {
                int xft_dpi_file = atoi(line + 8);
                if(xft_dpi_file > 0) {
                    xft_dpi = xft_dpi_file;
                    break;
                }
            }
        }

        pclose(xrdb_query);
        return xft_dpi;
    }

    float get_ui_scale() {
        if(scale_set)
            return scale;

        char *gdk_scale = getenv("GDK_SCALE");
        if(gdk_scale) {
            setlocale(LC_ALL, "C"); // Sigh... stupid C
            scale = atof(gdk_scale);
            if(scale < 0.0001f)
                scale = 1.0f;
        } else {
            scale = (float)xrdb_get_dpi() / (float)XFT_DPI_DEFAULT;
        }

        scale_set = true;
        return scale;
    }

    void show_virtual_keyboard() {
        if(!is_touch_enabled())
            return;

        fprintf(stderr, "Show virtual keyboard\n");
        system("busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b true");
    }

    void hide_virtual_keyboard() {
        if(!is_touch_enabled())
            return;

        fprintf(stderr, "Hide virtual keyboard\n");
        system("busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b false");
    }

    bool is_touch_enabled() {
        if(qm_enable_touch_set)
            return qm_enable_touch;

        const char *qm_enable_touch_env = getenv("QM_ENABLE_TOUCH");
        if(qm_enable_touch_env && qm_enable_touch_env[0] == '1')
            qm_enable_touch = true;

        qm_enable_touch_set = true;
        return qm_enable_touch;
    }
}