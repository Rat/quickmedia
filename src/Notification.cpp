#include "../include/Notification.hpp"
#include "../include/Program.hpp"
#include <assert.h>
#include <stdio.h>

namespace QuickMedia {
    const char* urgency_string(Urgency urgency) {
        switch(urgency) {
            case Urgency::LOW:
                return "low";
            case Urgency::NORMAL:
                return "normal";
            case Urgency::CRITICAL:
                return "critical";
        }
        assert(false);
        return nullptr;
    }

    void show_notification(const std::string &title, const std::string &description, Urgency urgency) {
        const char *args[] = { "notify-send", "-t", "10000", "-u", urgency_string(urgency), "--", title.c_str(), description.c_str(), nullptr };
        exec_program_async(args, nullptr);
        fprintf(stderr, "Notification: title: %s, description: %s\n", title.c_str(), description.c_str());
    }
}