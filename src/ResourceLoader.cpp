#include "../include/ResourceLoader.hpp"
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <array>
#include <unordered_map>
#include <malloc.h>
#include <assert.h>

static std::string resource_root;
static std::array<std::unique_ptr<sf::Font>, 3> font_cache;
static std::unordered_map<std::string, std::unique_ptr<sf::Texture>> texture_cache;

namespace QuickMedia {
    void set_resource_loader_root_path(const char *new_resource_root) {
        resource_root = new_resource_root;
    }
}

namespace QuickMedia::FontLoader {
    sf::Font* get_font(FontType font_type) {
        sf::Font *font = font_cache[(size_t)font_type].get();
        if(!font) {
            auto new_font = std::make_unique<sf::Font>();
            std::vector<std::string> noto_directories;
            std::string font_file_name;
            switch(font_type) {
                case FontType::LATIN: {
                    noto_directories.push_back("/usr/share/fonts/OTB");
                    noto_directories.push_back("/usr/share/fonts/noto");
                    font_file_name = "UW-Ttyp0.otb";
                    break;
                }
                case FontType::LATIN_BOLD: {
                    noto_directories.push_back("/usr/share/fonts/OTB");
                    noto_directories.push_back("/usr/share/fonts/truetype/noto");
                    font_file_name = "UW-Ttyp0-Bold.otb";
                    break;
                }
                case FontType::CJK: {
                    noto_directories.push_back("/usr/share/fonts/OTB");
                    noto_directories.push_back("/usr/share/fonts/truetype/noto-cjk");
                    font_file_name = "UW-Ttyp0.otb";
                    break;
                }
                case FontType::EMOJI: {
                    noto_directories.push_back("/usr/share/fonts/noto");
                    noto_directories.push_back("/usr/share/fonts/truetype/noto");
                    font_file_name = "NotoColorEmoji.ttf";
                    break;
                }
            }

            for(const std::string &noto_dir : noto_directories) {
                if(new_font->loadFromFile(noto_dir + "/" + font_file_name))
                    break;
            }

            font = new_font.get();
            font_cache[(size_t)font_type] = std::move(new_font);
            malloc_trim(0);
        }
        return font;
    }
}

namespace QuickMedia::TextureLoader {
    sf::Texture* get_texture(const char *filepath) {
        assert(!resource_root.empty());
        std::string str = filepath;
        auto it = texture_cache.find(str);
        if(it != texture_cache.end())
            return it->second.get();
        
        auto new_texture = std::make_unique<sf::Texture>();
        sf::Texture *result = new_texture.get();
        if(!new_texture->loadFromFile(resource_root + str))
            fprintf(stderr, "Failed to load image: %s%s\n", resource_root.c_str(), filepath);

        new_texture->setSmooth(true);
        texture_cache[str] = std::move(new_texture);
        malloc_trim(0);
        return result;
    }
}
