#include "../../plugins/Pipe.hpp"
#include "../../include/QuickMedia.hpp"
#include <string>
#include <iostream>

namespace QuickMedia {
    PluginResult PipePage::submit(const std::string &title, const std::string &url, std::vector<Tab>&) {
        puts(!url.empty() ? url.c_str() : title.c_str());
        program->set_pipe_selected_text(!url.empty() ? url : title);
        return PluginResult::OK;
    }

    // static
    void PipePage::load_body_items_from_stdin(BodyItems &items) {
        std::string line;
        while(std::getline(std::cin, line)) {
            std::string name;
            std::string filepath;
            size_t split_index = line.find('|');
            if(split_index == std::string::npos) {
                name = std::move(line);
            } else {
                name = line.substr(0, split_index);
                filepath = line.substr(split_index + 1);
            }

            auto body_item = BodyItem::create(std::move(name));
            if(!filepath.empty()) {
                body_item->thumbnail_url = std::move(filepath);
                body_item->thumbnail_is_local = true;
            }
            items.push_back(std::move(body_item));
        }
    }
}