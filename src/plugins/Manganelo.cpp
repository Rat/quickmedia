#include "../../plugins/Manganelo.hpp"
#include "../../include/Notification.hpp"
#include "../../include/StringUtils.hpp"
#include "../../include/NetUtils.hpp"
#include <quickmedia/HtmlSearch.h>

namespace QuickMedia {
    // Returns true if modified
    static bool remove_html_span(std::string &str) {
        size_t open_tag_start = str.find("<span");
        if(open_tag_start == std::string::npos)
            return false;

        size_t open_tag_end = str.find('>', open_tag_start + 5);
        if(open_tag_end == std::string::npos)
            return false;

        str.erase(open_tag_start, open_tag_end - open_tag_start + 1);

        size_t close_tag = str.find("</span>");
        if(close_tag == std::string::npos)
            return true;

        str.erase(close_tag, 7);
        return true;
    }

    static PluginResult submit_manga(Page *page, const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) {
        BodyItems chapters_items;
        std::vector<Creator> creators;

        std::string website_data;
        if(download_to_string(url, website_data, {}, true) != DownloadResult::OK)
            return PluginResult::NET_ERR;

        QuickMediaHtmlSearch html_search;
        int result = quickmedia_html_search_init(&html_search, website_data.c_str());
        if(result != 0)
            goto cleanup;

        result = quickmedia_html_find_nodes_xpath(&html_search, "//ul[class='row-content-chapter']//a",
            [](QuickMediaHtmlNode *node, void *userdata) {
                auto *item_data = (BodyItems*)userdata;
                const char *href = quickmedia_html_node_get_attribute_value(node, "href");
                const char *text = quickmedia_html_node_get_text(node);
                if(href && text) {
                    auto item = BodyItem::create(strip(text));
                    item->url = href;
                    item_data->push_back(std::move(item));
                }
            }, &chapters_items);

        BodyItemContext body_item_context;
        body_item_context.body_items = &chapters_items;
        body_item_context.index = 0;

        quickmedia_html_find_nodes_xpath(&html_search, "//ul[class='row-content-chapter']//span",
            [](QuickMediaHtmlNode *node, void *userdata) {
                auto *item_data = (BodyItemContext*)userdata;
                const char *class_attr = quickmedia_html_node_get_attribute_value(node, "class");
                const char *text = quickmedia_html_node_get_text(node);
                if(text && class_attr && strstr(class_attr, "chapter-time") && item_data->index < item_data->body_items->size()) {
                    std::string uploaded_date = strip(text);
                    (*item_data->body_items)[item_data->index]->set_description("Uploaded: " + uploaded_date);
                    item_data->index++;
                }
            }, &body_item_context);
        
        quickmedia_html_find_nodes_xpath(&html_search, "//a[class='a-h']",
            [](QuickMediaHtmlNode *node, void *userdata) {
                std::vector<Creator> *creators = (std::vector<Creator>*)userdata;
                const char *href = quickmedia_html_node_get_attribute_value(node, "href");
                const char *text = quickmedia_html_node_get_text(node);
                if(href && text && strstr(href, "/author/story/")) {
                    Creator creator;
                    creator.name = strip(text);
                    creator.url = href;
                    creators->push_back(std::move(creator));
                }
            }, &creators);

        cleanup:
        quickmedia_html_search_deinit(&html_search);
        if(result != 0)
            return PluginResult::ERR;

        auto chapters_body = page->create_body();
        chapters_body->items = std::move(chapters_items);
        result_tabs.push_back(Tab{std::move(chapters_body), std::make_unique<ManganeloChaptersPage>(page->program, title, url), page->create_search_bar("Search...", SEARCH_DELAY_FILTER)});

        for(Creator &creator : creators) {
            result_tabs.push_back(Tab{page->create_body(), std::make_unique<ManganeloCreatorPage>(page->program, std::move(creator)), page->create_search_bar("Search...", SEARCH_DELAY_FILTER)});
        }

        return PluginResult::OK;
    }

    SearchResult ManganeloSearchPage::search(const std::string &str, BodyItems &result_items) {
        std::string url = "https://manganelo.com/getstorysearchjson";
        std::string search_term = "searchword=";
        search_term += url_param_encode(str);
        CommandArg data_arg = { "--data", std::move(search_term) };

        Json::Value json_root;
        DownloadResult result = download_json(json_root, url, {data_arg}, true);
        if(result != DownloadResult::OK) return download_result_to_search_result(result);

        if(json_root.isNull())
            return SearchResult::OK;

        if(!json_root.isArray())
            return SearchResult::ERR;

        for(const Json::Value &child : json_root) {
            if(!child.isObject())
                continue;

            Json::Value name = child.get("name", "");
            Json::Value nameunsigned = child.get("nameunsigned", "");
            if(name.isString() && name.asCString()[0] != '\0' && nameunsigned.isString() && nameunsigned.asCString()[0] != '\0') {
                std::string name_str = name.asString();
                while(remove_html_span(name_str)) {}
                auto item = BodyItem::create(strip(name_str));
                item->url = "https://manganelo.com/manga/" + url_param_encode(nameunsigned.asString());
                Json::Value image = child.get("image", "");
                if(image.isString() && image.asCString()[0] != '\0')
                    item->thumbnail_url = image.asString();
                result_items.push_back(std::move(item));
            }
        }

        return SearchResult::OK;
    }

    PluginResult ManganeloSearchPage::submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) {
        return submit_manga(this, title, url, result_tabs);
    }

    PluginResult ManganeloChaptersPage::submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) {
        result_tabs.push_back(Tab{nullptr, std::make_unique<ManganeloImagesPage>(program, content_title, title, url), nullptr});
        return PluginResult::OK;
    }

    bool ManganeloChaptersPage::extract_id_from_url(const std::string &url, std::string &manga_id) const {
        bool manganelo_website = false;
        if(url.find("mangakakalot") != std::string::npos || url.find("manganelo") != std::string::npos)
            manganelo_website = true;

        if(manganelo_website) {
            size_t index = url.find("manga/");
            if(index == std::string::npos) {
                std::string err_msg = "Url ";
                err_msg += url;
                err_msg += " doesn't contain manga id";
                show_notification("QuickMedia", err_msg, Urgency::CRITICAL);
                return false;
            }

            manga_id = url.substr(index + 6);
            if(manga_id.size() <= 2) {
                std::string err_msg = "Url ";
                err_msg += url;
                err_msg += " doesn't contain manga id";
                show_notification("QuickMedia", err_msg, Urgency::CRITICAL);
                return false;
            }
            return true;
        } else {
            std::string err_msg = "Unexpected url ";
            err_msg += url;
            err_msg += " is not manganelo or mangakakalot";
            show_notification("QuickMedia", err_msg, Urgency::CRITICAL);
            return false;
        }
    }

    PluginResult ManganeloCreatorPage::submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) {
        return submit_manga(this, title, url, result_tabs);
    }

    PluginResult ManganeloCreatorPage::lazy_fetch(BodyItems &result_items) {
        std::string website_data;
        if(download_to_string(creator.url, website_data, {}, true) != DownloadResult::OK)
            return PluginResult::NET_ERR;

        QuickMediaHtmlSearch html_search;
        int result = quickmedia_html_search_init(&html_search, website_data.c_str());
        if(result != 0)
            goto cleanup;

        result = quickmedia_html_find_nodes_xpath(&html_search, "//div[class='search-story-item']//a[class='item-img']",
            [](QuickMediaHtmlNode *node, void *userdata) {
                auto *item_data = (BodyItems*)userdata;
                const char *href = quickmedia_html_node_get_attribute_value(node, "href");
                const char *title = quickmedia_html_node_get_attribute_value(node, "title");
                if(href && title && strstr(href, "/manga/")) {
                    auto body_item = BodyItem::create(title);
                    body_item->url = href;
                    item_data->push_back(std::move(body_item));
                }
            }, &result_items);

        if(result != 0)
            goto cleanup;

        BodyItemContext body_item_image_context;
        body_item_image_context.body_items = &result_items;
        body_item_image_context.index = 0;

        result = quickmedia_html_find_nodes_xpath(&html_search, "//div[class='search-story-item']//a[class='item-img']//img",
            [](QuickMediaHtmlNode *node, void *userdata) {
                auto *item_data = (BodyItemContext*)userdata;
                const char *src = quickmedia_html_node_get_attribute_value(node, "src");
                if(src && item_data->index < item_data->body_items->size()) {
                    (*item_data->body_items)[item_data->index]->thumbnail_url = src;
                    item_data->index++;
                }
            }, &body_item_image_context);

        cleanup:
        quickmedia_html_search_deinit(&html_search);
        if(result != 0) {
            result_items.clear();
            return PluginResult::ERR;
        }
        return PluginResult::OK;
    }

    ImageResult ManganeloImagesPage::get_number_of_images(int &num_images) {
        num_images = 0;
        ImageResult image_result = get_image_urls_for_chapter(url);
        if(image_result != ImageResult::OK)
            return image_result;

        num_images = chapter_image_urls.size();
        return ImageResult::OK;
    }

    ImageResult ManganeloImagesPage::for_each_page_in_chapter(PageCallback callback) {
        std::vector<std::string> image_urls;
        ImageResult image_result = get_image_urls_for_chapter(url);
        if(image_result != ImageResult::OK)
            return image_result;

        image_urls = chapter_image_urls;

        for(const std::string &url : image_urls) {
            if(!callback(url))
                break;
        }

        return ImageResult::OK;
    }

    ImageResult ManganeloImagesPage::get_image_urls_for_chapter(const std::string &url) {
        if(!chapter_image_urls.empty())
            return ImageResult::OK;

        std::string website_data;
        if(download_to_string(url, website_data, {}, true) != DownloadResult::OK)
            return ImageResult::NET_ERR;

        QuickMediaHtmlSearch html_search;
        int result = quickmedia_html_search_init(&html_search, website_data.c_str());
        if(result != 0)
            goto cleanup;

        result = quickmedia_html_find_nodes_xpath(&html_search, "//div[class='container-chapter-reader']/img",
            [](QuickMediaHtmlNode *node, void *userdata) {
                auto *urls = (std::vector<std::string>*)userdata;
                const char *src = quickmedia_html_node_get_attribute_value(node, "src");
                if(src) {
                    std::string image_url = src;
                    urls->push_back(std::move(image_url));
                }
            }, &chapter_image_urls);

        cleanup:
        quickmedia_html_search_deinit(&html_search);
        if(result != 0) {
            chapter_image_urls.clear();
            return ImageResult::ERR;
        }
        if(chapter_image_urls.empty())
            return ImageResult::ERR;
        return ImageResult::OK;
    }
}