#include "../../plugins/Plugin.hpp"

namespace QuickMedia {
    SuggestionResult download_result_to_suggestion_result(DownloadResult download_result) { return (SuggestionResult)download_result; }
    PluginResult download_result_to_plugin_result(DownloadResult download_result) { return (PluginResult)download_result; }
    SearchResult download_result_to_search_result(DownloadResult download_result) { return (SearchResult)download_result; }
    ImageResult download_result_to_image_result(DownloadResult download_result) { return (ImageResult)download_result; }
    PluginResult search_result_to_plugin_result(SearchResult search_result) { return (PluginResult)search_result; }
    SearchResult plugin_result_to_search_result(PluginResult plugin_result) { return (SearchResult)plugin_result; }
}