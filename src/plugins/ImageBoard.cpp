#include "../../plugins/ImageBoard.hpp"

namespace QuickMedia {
    BodyItems ImageBoardThreadPage::get_related_media(const std::string &url, std::string&) {
        BodyItems body_items;
        auto it = std::find(cached_media_urls.begin(), cached_media_urls.end(), url);
        if(it == cached_media_urls.end())
            return body_items;

        ++it;
        for(; it != cached_media_urls.end(); ++it) {
            auto body_item = BodyItem::create("");
            body_item->url = *it;
            body_items.push_back(std::move(body_item));
        }
        return body_items;
    }

    std::unique_ptr<RelatedVideosPage> ImageBoardThreadPage::create_related_videos_page(Program*, const std::string&, const std::string&) {
        return nullptr;
    }

    PluginResult ImageBoardThreadPage::login(const std::string &token, const std::string &pin, std::string &response_msg) {
        (void)token;
        (void)pin;
        response_msg = "Login is not supported on this image board";
        return PluginResult::ERR;
    }

    const std::string& ImageBoardThreadPage::get_pass_id() {
        static std::string empty_str;
        return empty_str;
    }
}