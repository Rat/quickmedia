#include "../../plugins/Page.hpp"
#include "../../include/QuickMedia.hpp"
#include <json/reader.h>

namespace QuickMedia {
    DownloadResult Page::download_json(Json::Value &result, const std::string &url, std::vector<CommandArg> additional_args, bool use_browser_useragent, std::string *err_msg) {
        std::string server_response;
        if(download_to_string(url, server_response, std::move(additional_args), use_browser_useragent, err_msg == nullptr) != DownloadResult::OK) {
            if(err_msg)
                *err_msg = server_response;
            return DownloadResult::NET_ERR;
        }

        if(server_response.empty()) {
            result = Json::Value::nullSingleton();
            return DownloadResult::OK;
        }

        Json::CharReaderBuilder json_builder;
        std::unique_ptr<Json::CharReader> json_reader(json_builder.newCharReader());
        std::string json_errors;
        if(!json_reader->parse(&server_response[0], &server_response[server_response.size()], &result, &json_errors)) {
            fprintf(stderr, "download_json error: %s\n", json_errors.c_str());
            if(err_msg)
                *err_msg = std::move(json_errors);
            return DownloadResult::ERR;
        }

        return DownloadResult::OK;
    }

    std::unique_ptr<Body> Page::create_body() {
        return program->create_body();
    }

    std::unique_ptr<SearchBar> Page::create_search_bar(const std::string &placeholder_text, int search_delay) {
        return program->create_search_bar(placeholder_text, search_delay);
    }

    bool Page::load_manga_content_storage(const char *service_name, const std::string &manga_title, const std::string &manga_id) {
        return program->load_manga_content_storage(service_name, manga_title, manga_id);
    }
}