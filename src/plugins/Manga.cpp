#include "../../plugins/Manga.hpp"
#include "../../include/Program.hpp"

namespace QuickMedia {
    TrackResult MangaChaptersPage::track(const std::string &str) {
        const char *args[] = { "automedia", "add", "html", content_url.data(), "--start-after", str.data(), "--name", content_title.data(), nullptr };
        if(exec_program(args, nullptr, nullptr) == 0)
            return TrackResult::OK;
        else
            return TrackResult::ERR;
    }

    void MangaChaptersPage::on_navigate_to_page(Body *body) {
        (void)body;
        std::string manga_id;
        if(extract_id_from_url(content_url, manga_id))
            load_manga_content_storage(get_service_name(), content_title, manga_id);
    }
}