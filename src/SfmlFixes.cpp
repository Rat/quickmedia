#include "../include/SfmlFixes.hpp"
#include <mutex>

static std::mutex mutex;
namespace QuickMedia {
    bool load_image_from_file(sf::Image &image, const std::string &filepath) {
        std::lock_guard<std::mutex> lock(mutex);
        return image.loadFromFile(filepath);
    }

    bool load_image_from_memory(sf::Image &image, const void *data, size_t size) {
        std::lock_guard<std::mutex> lock(mutex);
        return image.loadFromMemory(data, size);
    }
}