#include "../include/DownloadUtils.hpp"
#include "../include/Program.hpp"
#include "../include/Storage.hpp"
#include "../include/base64_url.hpp"
#include <SFML/System/Clock.hpp>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

static const bool debug_download = false;

static int accumulate_string(char *data, int size, void *userdata) {
    std::string *str = (std::string*)userdata;
    if(str->size() + size > 1024 * 1024 * 100) // 100mb sane limit, TODO: make configurable
        return 1;
    str->append(data, size);
    return 0;
}

static const char *useragent_str = "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36";

namespace QuickMedia {
    // TODO: Add timeout
    DownloadResult download_to_string(const std::string &url, std::string &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent, bool fail_on_error) {
        sf::Clock timer;
        std::vector<const char*> args;
        args.insert(args.end(), { "curl", "-H", "Accept-Language: en-US,en;q=0.5", "-H", "Connection: keep-alive", "--compressed", "-s", "-L" });
        if(fail_on_error)
            args.push_back("-f");
        for(const CommandArg &arg : additional_args) {
            args.push_back(arg.option.c_str());
            args.push_back(arg.value.c_str());
        }
        if(use_browser_useragent) {
            args.push_back("-H");
            args.push_back(useragent_str);
        }
        args.push_back("--");
        args.push_back(url.c_str());
        args.push_back(nullptr);
        if(debug_download) {
            for(const char *arg : args) {
                if(arg)
                    fprintf(stderr, "'%s' ", arg);
            }
            fprintf(stderr, "\n");
        }
        if(exec_program(args.data(), accumulate_string, &result) != 0)
            return DownloadResult::NET_ERR;
        fprintf(stderr, "Download duration for %s: %d ms\n", url.c_str(), timer.getElapsedTime().asMilliseconds());
        return DownloadResult::OK;
    }

    DownloadResult download_to_string_cache(const std::string &url, std::string &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent, DownloadErrorHandler error_handler, Path cache_path) {
        Path media_dir;
        Path media_file_path;
        if(cache_path.data.empty()) {
            media_dir = get_cache_dir().join("media");
            media_file_path = Path(media_dir).join(base64_url::encode(url));
        } else {
            media_dir = cache_path.parent();
            media_file_path = std::move(cache_path);
        }
        if(get_file_type(media_file_path) == FileType::REGULAR) {
            if(file_get_content(media_file_path, result) == 0) {
                fprintf(stderr, "Loaded %s from cache\n", url.c_str());
                return DownloadResult::OK;
            } else {
                fprintf(stderr, "Failed to get content of cached media file: %s\n", media_file_path.data.c_str());
                return DownloadResult::ERR;
            }
        } else {
            DownloadResult download_result = download_to_string(url, result, additional_args, use_browser_useragent, error_handler ? false : true);
            if(download_result == DownloadResult::OK && error_handler)
                download_result = error_handler(result) ? DownloadResult::OK : DownloadResult::ERR;
            if(download_result == DownloadResult::OK) {
                Path media_file_path_tmp(media_file_path.data + ".tmp");
                if(create_directory_recursive(media_dir) == 0 && file_overwrite(media_file_path_tmp, result) == 0) {
                    if(rename(media_file_path_tmp.data.c_str(), media_file_path.data.c_str()) != 0) {
                        perror("rename");
                        download_result = DownloadResult::ERR;
                    }
                } else {
                    download_result = DownloadResult::ERR;
                }
            }
            return download_result;
        }
    }

    // TODO: Use this everywhere we want to save to file (such as manga download)
    DownloadResult download_to_file(const std::string &url, const std::string &destination_filepath, const std::vector<CommandArg> &additional_args, bool use_browser_useragent) {
        Path tmp_filepath = destination_filepath;
        tmp_filepath.append(".tmp");

        // TODO: Optimize with temporary '\0'
        size_t dir_end = tmp_filepath.data.rfind('/');
        if(dir_end != std::string::npos && create_directory_recursive(tmp_filepath.data.substr(0, dir_end)) != 0)
            return DownloadResult::ERR;

        std::vector<CommandArg> args = additional_args;
        args.push_back({ "-o", tmp_filepath.data.c_str() });

        std::string dummy;
        DownloadResult res = download_to_string(url, dummy, std::move(args), use_browser_useragent);
        if(res != DownloadResult::OK) return res;
        
        if(rename(tmp_filepath.data.c_str(), destination_filepath.c_str()) != 0) {
            perror("rename");
            return DownloadResult::ERR;
        }

        return DownloadResult::OK;
    }

    // TODO: Add timeout
    DownloadResult download_to_json(const std::string &url, rapidjson::Document &result, const std::vector<CommandArg> &additional_args, bool use_browser_useragent, bool fail_on_error) {
        sf::Clock timer;
        std::vector<const char*> args;
        args.insert(args.end(), { "curl", "-H", "Accept-Language: en-US,en;q=0.5", "-H", "Connection: keep-alive", "--compressed", "-s", "-L" });
        if(fail_on_error)
            args.push_back("-f");
        for(const CommandArg &arg : additional_args) {
            args.push_back(arg.option.c_str());
            args.push_back(arg.value.c_str());
        }
        if(use_browser_useragent) {
            args.push_back("-H");
            args.push_back(useragent_str);
        }
        args.push_back("--");
        args.push_back(url.c_str());
        args.push_back(nullptr);
        if(debug_download) {
            for(const char *arg : args) {
                if(arg)
                    fprintf(stderr, "'%s' ", arg);
            }
            fprintf(stderr, "\n");
        }

        ReadProgram read_program;
        if(exec_program_pipe(args.data(), &read_program) != 0)
            return DownloadResult::NET_ERR;
        
        FILE *file = fdopen(read_program.read_fd, "rb");
        char read_buffer[8192];
        rapidjson::FileReadStream is(file, read_buffer, sizeof(read_buffer));
        rapidjson::ParseResult parse_result = result.ParseStream(is);
        program_clear_current_thread();

        fclose(file);
        wait_program(read_program.pid);
        fprintf(stderr, "Download duration for %s: %d ms\n", url.c_str(), timer.getElapsedTime().asMilliseconds());
        return parse_result.IsError() ? DownloadResult::ERR : DownloadResult::OK;
    }
}