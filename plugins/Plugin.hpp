#pragma once

#include "../include/Body.hpp"
#include "../include/DownloadUtils.hpp"
#include <stddef.h>

namespace QuickMedia {
    enum class PluginResult {
        OK,
        ERR,
        NET_ERR
    };

    enum class SearchResult {
        OK,
        ERR,
        NET_ERR
    };

    enum class SuggestionResult {
        OK,
        ERR,
        NET_ERR
    };

    enum class ImageResult {
        OK,
        END,
        ERR,
        NET_ERR
    };

    struct BodyItemContext {
        BodyItems *body_items;
        size_t index;
    };

    SuggestionResult download_result_to_suggestion_result(DownloadResult download_result);
    PluginResult download_result_to_plugin_result(DownloadResult download_result);
    SearchResult download_result_to_search_result(DownloadResult download_result);
    ImageResult download_result_to_image_result(DownloadResult download_result);
    PluginResult search_result_to_plugin_result(SearchResult search_result);
    SearchResult plugin_result_to_search_result(PluginResult plugin_result);
}