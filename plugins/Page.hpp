#pragma once

#include "Plugin.hpp"

#include "../include/Tab.hpp"
#include "../include/SearchBar.hpp"
#include "../include/Body.hpp"

namespace QuickMedia {
    constexpr int SEARCH_DELAY_FILTER = 50;

    // TODO: Remove to PageType when the other PageType is removed
    enum class PageTypez {
        REGULAR,
        MANGA_IMAGES,
        IMAGE_BOARD_THREAD,
        VIDEO,
        CHAT
    };

    class Page {
    public:
        Page(Program *program) : program(program) {}
        virtual ~Page() = default;

        virtual const char* get_title() const = 0;
        virtual bool search_is_filter() { return true; }
        // This show be overriden if search_is_filter is overriden to return false
        virtual SearchResult search(const std::string &str, BodyItems &result_items) { (void)str; (void)result_items; return SearchResult::ERR; }

        // Return empty |result_tabs| and PluginResult::OK to do nothing; which is useful for implementing custom actions on item submit
        virtual PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) {
            (void)title;
            (void)url;
            (void)result_tabs;
            return PluginResult::ERR;
        }
        virtual bool clear_search_after_submit() { return false; }
        // Note: If pagination is done by fetching the next page until we get to |page|, then the "current page" should be reset everytime |search| is called.
        // Note: the first page is 0
        virtual PluginResult get_page(const std::string &str, int page, BodyItems &result_items) { (void)str; (void)page; (void)result_items; return PluginResult::OK; }

        DownloadResult download_json(Json::Value &result, const std::string &url, std::vector<CommandArg> additional_args, bool use_browser_useragent = false, std::string *err_msg = nullptr);

        virtual PageTypez get_type() const { return PageTypez::REGULAR; }
        // Mutually exclusive with |get_type| when |get_type| is not PageTypez::REGULAR
        virtual bool is_single_page() const { return false; }
        virtual bool is_trackable() const { return false; }
        virtual bool is_lazy_fetch_page() const { return false; }
        // Note: If submit is done without any selection, then the search term is sent as the |title|, not |url|
        virtual bool allow_submit_no_selection() const { return false; }

        // This is called both when first navigating to page and when going back to page
        virtual void on_navigate_to_page(Body *body) { (void)body; }

        // Called periodically (every frame right now) if this page is the currently active one
        virtual void update() {}

        std::unique_ptr<Body> create_body();
        std::unique_ptr<SearchBar> create_search_bar(const std::string &placeholder_text, int search_delay);

        bool load_manga_content_storage(const char *service_name, const std::string &manga_title, const std::string &manga_id);

        virtual sf::Vector2i get_thumbnail_max_size() { return sf::Vector2i(250, 141); };
    
        Program *program;
        std::shared_ptr<BodyItem> submit_body_item; // TODO: Remove this
    };

    enum class TrackResult {
        OK,
        ERR
    };

    class TrackablePage : public Page {
    public:
        TrackablePage(Program *program, std::string content_title, std::string content_url) : Page(program), content_title(std::move(content_title)), content_url(std::move(content_url)) {}
        const char* get_title() const override { return content_title.c_str(); }
        bool is_trackable() const override { return true; }
        virtual TrackResult track(const std::string &str) = 0;

        const std::string content_title;
        const std::string content_url;
    };

    class LazyFetchPage : public Page {
    public:
        LazyFetchPage(Program *program) : Page(program) {}
        virtual bool search_is_filter() override { return true; }
        bool is_lazy_fetch_page() const override { return true; }
        virtual PluginResult lazy_fetch(BodyItems &result_items) = 0;
    };

    class RelatedVideosPage : public Page {
    public:
        RelatedVideosPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Related videos"; }
    };

    class VideoPage : public Page {
    public:
        VideoPage(Program *program) : Page(program) {}
        virtual PageTypez get_type() const override { return PageTypez::VIDEO; }
        virtual bool autoplay_next_item() { return false; }
        virtual BodyItems get_related_media(const std::string &url, std::string &channel_url) { (void)url; (void)channel_url; return {}; }
        virtual std::unique_ptr<Page> create_search_page(Program *program, int &search_delay) { (void)program; (void)search_delay; return nullptr; }
        virtual std::unique_ptr<Page> create_comments_page(Program *program) { (void)program; return nullptr; }
        // Return nullptr if the service doesn't support related videos page
        virtual std::unique_ptr<RelatedVideosPage> create_related_videos_page(Program *program, const std::string &video_url, const std::string &video_title) = 0;
        // Return nullptr if the service doesn't support channels page
        virtual std::unique_ptr<LazyFetchPage> create_channels_page(Program *program, const std::string &channel_url) = 0;
        virtual std::string get_url() = 0;
        virtual std::string url_get_playable_url(const std::string &url) { return url; }
        virtual bool video_should_be_skipped(const std::string &url) { (void)url; return false; }
    };
}