#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class NyaaSiCategoryPage : public Page {
    public:
        NyaaSiCategoryPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Select category"; }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        bool clear_search_after_submit() override { return true; }
        void get_categories(BodyItems &result_items);
    };

    class NyaaSiSearchPage : public Page {
    public:
        NyaaSiSearchPage(Program *program, std::string category_name, std::string category_id) : Page(program), category_name(std::move(category_name)), category_id(std::move(category_id)) {}
        const char* get_title() const override { return category_name.c_str(); }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;

        const std::string category_name;
        const std::string category_id;
    };

    class NyaaSiTorrentPage : public Page {
    public:
        NyaaSiTorrentPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "Torrent"; }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    };
}