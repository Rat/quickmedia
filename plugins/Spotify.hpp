#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class SpotifyPage : public Page {
    public:
        SpotifyPage(Program *program);
        virtual ~SpotifyPage() = default;
    protected:
        DownloadResult download_json_error_retry(Json::Value &json_root, const std::string &url, std::vector<CommandArg> additional_args);
    private:
        PluginResult update_token();
    private:
        std::string access_token;
    };

    class SpotifyPodcastSearchPage : public SpotifyPage {
    public:
        SpotifyPodcastSearchPage(Program *program) : SpotifyPage(program) {}
        const char* get_title() const override { return "Podcasts"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    };

    class SpotifyEpisodeListPage : public SpotifyPage {
    public:
        SpotifyEpisodeListPage(Program *program, const std::string &url) : SpotifyPage(program), url(url) {}
        const char* get_title() const override { return "Episodes"; }
        bool search_is_filter() override { return true; }
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    private:
        std::string url;
    };

    class SpotifyAudioPage : public VideoPage {
    public:
        SpotifyAudioPage(Program *program, const std::string &url) : VideoPage(program), url(url) {}
        const char* get_title() const override { return ""; }
        std::unique_ptr<RelatedVideosPage> create_related_videos_page(Program *, const std::string &, const std::string &) override { return nullptr; }
        std::unique_ptr<LazyFetchPage> create_channels_page(Program *, const std::string &)  override { return nullptr; }
        std::string get_url() override { return url; }
    private:
        std::string url;
    };
}