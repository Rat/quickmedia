#pragma once

#include "Page.hpp"
#include <functional>

namespace QuickMedia {
    // Return false to stop iteration
    using PageCallback = std::function<bool(const std::string &url)>;

    struct Creator {
        std::string name;
        std::string url;
    };

    class MangaImagesPage : public Page {
    public:
        MangaImagesPage(Program *program, std::string manga_name, std::string chapter_name, std::string url) : Page(program), manga_name(std::move(manga_name)), chapter_name(std::move(chapter_name)), url(std::move(url)) {}
        virtual ~MangaImagesPage() = default;
        const char* get_title() const override { return chapter_name.c_str(); }
        PageTypez get_type() const override { return PageTypez::MANGA_IMAGES; }

        virtual ImageResult get_number_of_images(int &num_images) = 0;
        virtual ImageResult for_each_page_in_chapter(PageCallback callback) = 0;

        virtual void change_chapter(std::string new_chapter_name, std::string new_url) {
            chapter_name = std::move(new_chapter_name);
            if(url != new_url) {
                url = std::move(new_url);
                chapter_image_urls.clear();
            }
        }

        const std::string& get_chapter_name() const { return chapter_name; }
        const std::string& get_url() const { return url; }

        virtual const char* get_service_name() const = 0;

        const std::string manga_name;
    protected:
        std::string chapter_name;
        std::string url;
        std::vector<std::string> chapter_image_urls;
    };

    class MangaChaptersPage : public TrackablePage {
    public:
        MangaChaptersPage(Program *program, std::string manga_name, std::string manga_url) : TrackablePage(program, std::move(manga_name), std::move(manga_url)) {}
        TrackResult track(const std::string &str) override;
        void on_navigate_to_page(Body *body) override;
    protected:
        virtual bool extract_id_from_url(const std::string &url, std::string &manga_id) const = 0;
        virtual const char* get_service_name() const = 0;
    };
}