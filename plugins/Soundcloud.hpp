#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class SoundcloudPage : public Page {
    public:
        SoundcloudPage(Program *program) : Page(program) {}
        virtual ~SoundcloudPage() = default;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    };

    class SoundcloudSearchPage : public SoundcloudPage {
    public:
        SoundcloudSearchPage(Program *program) : SoundcloudPage(program) {}
        const char* get_title() const override { return "Search"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
    private:
        std::string query_urn;
    };

    class SoundcloudUserPage : public SoundcloudPage {
    public:
        SoundcloudUserPage(Program *program, const std::string &username, const std::string &userpage_url, std::string next_href) : SoundcloudPage(program), username(username), userpage_url(userpage_url), next_href(std::move(next_href)), current_page(0) {}
        const char* get_title() const override { return username.c_str(); }
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
    private:
        PluginResult get_continuation_page(BodyItems &result_items);
    private:
        std::string username;
        std::string userpage_url;
        std::string next_href;
        int current_page;
    };

    class SoundcloudPlaylistPage : public SoundcloudPage {
    public:
        SoundcloudPlaylistPage(Program *program, const std::string &playlist_name) : SoundcloudPage(program), playlist_name(playlist_name) {}
        const char* get_title() const override { return playlist_name.c_str(); }
    private:
        std::string playlist_name;
    };

    class SoundcloudAudioPage : public VideoPage {
    public:
        SoundcloudAudioPage(Program *program, const std::string &url) : VideoPage(program), url(url) {}
        const char* get_title() const override { return ""; }
        bool autoplay_next_item() override { return true; }
        std::unique_ptr<RelatedVideosPage> create_related_videos_page(Program *, const std::string &, const std::string &) override { return nullptr; }
        std::unique_ptr<LazyFetchPage> create_channels_page(Program *, const std::string &)  override { return nullptr; }
        std::string get_url() override { return url; }
        std::string url_get_playable_url(const std::string &url) override;
        bool video_should_be_skipped(const std::string &url) override { return url == "track"; }
    private:
        std::string url;
    };
}