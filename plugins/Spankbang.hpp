#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class SpankbangSearchPage : public Page {
    public:
        SpankbangSearchPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "All"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    };

    class SpankbangRelatedVideosPage : public RelatedVideosPage {
    public:
        SpankbangRelatedVideosPage(Program *program) : RelatedVideosPage(program) {}
        PluginResult submit(const std::string&, const std::string&, std::vector<Tab> &result_tabs) override;
    };

    class SpankbangVideoPage : public VideoPage {
    public:
        SpankbangVideoPage(Program *program, const std::string &url) : VideoPage(program), url(url) {}
        const char* get_title() const override { return ""; }
        BodyItems get_related_media(const std::string &url, std::string &channel_url) override;
        std::unique_ptr<Page> create_search_page(Program *program, int &search_delay) override;
        std::unique_ptr<RelatedVideosPage> create_related_videos_page(Program *program, const std::string &video_url, const std::string &video_title) override;
        std::unique_ptr<LazyFetchPage> create_channels_page(Program*, const std::string&) override {
            return nullptr;
        }
        std::string get_url() override { return url; }
    private:
        std::string url;
    };
}