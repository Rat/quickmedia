#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class Pleroma {
    public:
        // Either get all home posts or only the new ones if we have already fetched posts once. TODO: Cache
        PluginResult get_home_posts(BodyItems &results, const std::string &max_id = "");
    private:
        std::string auth_token = "";
        std::string instance = "";
    };

    class PleromaHomePage : public LazyFetchPage {
    public:
        PleromaHomePage(Program *program, std::shared_ptr<Pleroma> pleroma) : LazyFetchPage(program), pleroma(pleroma) {}
        const char* get_title() const override { return "Home"; }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override {
            return PluginResult::ERR;
        }
        PluginResult lazy_fetch(BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
    private:
        std::shared_ptr<Pleroma> pleroma;
        int current_page = 0;
        std::string last_item_id;
    };
}