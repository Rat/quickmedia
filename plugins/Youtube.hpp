#pragma once

#include "Page.hpp"
#include <unordered_set>

namespace QuickMedia {
    class YoutubeSearchPage : public Page {
    public:
        YoutubeSearchPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "All"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    private:
        PluginResult search_get_continuation(const std::string &url, const std::string &continuation_token, BodyItems &result_items);
    private:
        std::string search_url;
        std::string continuation_token;
        int current_page = 0;
        std::unordered_set<std::string> added_videos;
    };

    class YoutubeCommentsPage : public LazyFetchPage {
    public:
        YoutubeCommentsPage(Program *program, const std::string &xsrf_token, const std::string &continuation_token) : LazyFetchPage(program), xsrf_token(xsrf_token), continuation_token(continuation_token) {}
        const char* get_title() const override { return "Comments"; }
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        int current_page = 0;
        std::string xsrf_token;
        std::string continuation_token;
    };

    class YoutubeCommentRepliesPage : public LazyFetchPage {
    public:
        YoutubeCommentRepliesPage(Program *program, const std::string &xsrf_token, const std::string &continuation_token) : LazyFetchPage(program), xsrf_token(xsrf_token), continuation_token(continuation_token) {}
        const char* get_title() const override { return "Comment replies"; }
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
    private:
        int current_page = 0;
        std::string xsrf_token;
        std::string continuation_token;
    };

    class YoutubeChannelPage : public LazyFetchPage {
    public:
        YoutubeChannelPage(Program *program, std::string url, std::string continuation_token, std::string title) : LazyFetchPage(program), url(std::move(url)), continuation_token(std::move(continuation_token)), title(std::move(title)) {}
        const char* get_title() const override { return title.c_str(); }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;

        std::unordered_set<std::string> added_videos;
    private:
        PluginResult search_get_continuation(const std::string &url, const std::string &continuation_token, BodyItems &result_items);
    private:
        const std::string url;
        std::string continuation_token;
        const std::string title;
        int current_page = 0;
    };

    class YoutubeRelatedVideosPage : public RelatedVideosPage {
    public:
        YoutubeRelatedVideosPage(Program *program) : RelatedVideosPage(program) {}
        PluginResult submit(const std::string&, const std::string&, std::vector<Tab> &result_tabs) override;
    };

    class YoutubeVideoPage : public VideoPage {
    public:
        YoutubeVideoPage(Program *program, const std::string &url) : VideoPage(program), url(url) {}
        const char* get_title() const override { return ""; }
        BodyItems get_related_media(const std::string &url, std::string &channel_url) override;
        std::unique_ptr<Page> create_search_page(Program *program, int &search_delay) override;
        std::unique_ptr<Page> create_comments_page(Program *program) override;
        std::unique_ptr<RelatedVideosPage> create_related_videos_page(Program *program, const std::string &video_url, const std::string &video_title) override;
        std::unique_ptr<LazyFetchPage> create_channels_page(Program *program, const std::string &channel_url) override;
        std::string get_url() override { return url; }
    private:
        std::string xsrf_token;
        std::string comments_continuation_token;
        std::string url;
    };
}