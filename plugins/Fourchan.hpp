#pragma once

#include "ImageBoard.hpp"

namespace QuickMedia {
    class FourchanBoardsPage : public Page {
    public:
        FourchanBoardsPage(Program *program, std::string resources_root) : Page(program), resources_root(std::move(resources_root)) {}
        const char* get_title() const override { return "Select board"; }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        bool clear_search_after_submit() override { return true; }
        void get_boards(BodyItems &result_items);

        const std::string resources_root;
    };

    class FourchanThreadListPage : public LazyFetchPage {
    public:
        FourchanThreadListPage(Program *program, std::string title, std::string board_id) : LazyFetchPage(program), title(std::move(title)), board_id(std::move(board_id)) {}
        const char* get_title() const override { return title.c_str(); }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;

        const std::string title;
        const std::string board_id;
    private:
        std::vector<std::string> cached_media_urls;
    };

    class FourchanThreadPage : public ImageBoardThreadPage {
    public:
        FourchanThreadPage(Program *program, std::string board_id, std::string thread_id, std::vector<std::string> cached_media_urls) : ImageBoardThreadPage(program, std::move(board_id), std::move(thread_id), std::move(cached_media_urls)) {}

        PluginResult login(const std::string &token, const std::string &pin, std::string &response_msg) override;
        PostResult post_comment(const std::string &captcha_id, const std::string &comment) override;
        const std::string& get_pass_id() override;
    private:
        std::string pass_id;
    };
}