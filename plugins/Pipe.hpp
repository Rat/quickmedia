#pragma once

#include "Page.hpp"

namespace QuickMedia {
    class PipePage : public Page {
    public:
        PipePage(Program *program, const char *title = "") : Page(program), title(title) {}
        const char* get_title() const override { return title; }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        bool is_single_page() const override { return true; }

        static void load_body_items_from_stdin(BodyItems &items);
    private:
        const char *title;
    };
}