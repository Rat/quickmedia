#pragma once

#include "Manga.hpp"
#include <functional>

namespace QuickMedia {
    class ManganeloSearchPage : public Page {
    public:
        ManganeloSearchPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "All"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        sf::Vector2i get_thumbnail_max_size() override { return sf::Vector2i(101, 141); };
    };

    class ManganeloChaptersPage : public MangaChaptersPage {
    public:
        ManganeloChaptersPage(Program *program, std::string manga_name, std::string manga_url) : MangaChaptersPage(program, std::move(manga_name), std::move(manga_url)) {}
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    protected:
        bool extract_id_from_url(const std::string &url, std::string &manga_id) const override;
        const char* get_service_name() const override { return "manganelo"; }
    };

    class ManganeloCreatorPage : public LazyFetchPage {
    public:
        ManganeloCreatorPage(Program *program, Creator creator) : LazyFetchPage(program), creator(std::move(creator)) {}
        const char* get_title() const override { return creator.name.c_str(); }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        PluginResult lazy_fetch(BodyItems &result_items) override;
        sf::Vector2i get_thumbnail_max_size() override { return sf::Vector2i(101, 141); };
    private:
        Creator creator;
    };

    class ManganeloImagesPage : public MangaImagesPage {
    public:
        ManganeloImagesPage(Program *program, std::string manga_name, std::string chapter_name, std::string url) : MangaImagesPage(program, std::move(manga_name), std::move(chapter_name), std::move(url)) {}

        ImageResult get_number_of_images(int &num_images) override;
        ImageResult for_each_page_in_chapter(PageCallback callback) override;

        const char* get_service_name() const override { return "manganelo"; }
    private:
        // Cached
        ImageResult get_image_urls_for_chapter(const std::string &url);
    };
}