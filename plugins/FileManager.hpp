#pragma once

#include "Page.hpp"
#include <filesystem>

namespace QuickMedia {
    class FileManagerPage : public Page {
    public:
        FileManagerPage(Program *program) : Page(program), current_dir("/") {}
        const char* get_title() const override { return current_dir.c_str(); }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        bool is_single_page() const override { return true; }

        bool set_current_directory(const std::string &path);
        PluginResult get_files_in_directory(BodyItems &result_items);
    private:
        std::filesystem::path current_dir;
    };
}