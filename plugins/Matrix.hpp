#pragma once

#include "../include/FileAnalyzer.hpp"
#include "Plugin.hpp"
#include "Page.hpp"
#include <SFML/Graphics/Color.hpp>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <mutex>
#include <rapidjson/fwd.h>

namespace QuickMedia {
    struct RoomData;
    struct Message;

    std::string remove_reply_formatting(const std::string &str);
    std::string message_get_body_remove_formatting(Message *message);

    enum class UserResolveState {
        NOT_RESOLVED,
        RESOLVING,
        RESOLVED
    };

    struct UserInfo {
        friend struct RoomData;
        UserInfo(RoomData *room, std::string user_id);
        UserInfo(RoomData *room, std::string user_id, std::string display_name, std::string avatar_url);

        RoomData *room;
        const sf::Color display_name_color;
        const std::string user_id;
        UserResolveState resolve_state;
    private:
        std::string display_name;
        std::string avatar_url;
        std::string read_marker_event_id;
    };

    enum class MessageType {
        TEXT,
        IMAGE,
        VIDEO,
        AUDIO,
        FILE,
        REDACTION,
        REACTION,
        MEMBERSHIP,
        UNIMPLEMENTED
    };

    bool is_visual_media_message_type(MessageType message_type);

    enum class RelatedEventType {
        NONE,
        REPLY,
        EDIT,
        REDACTION,
        REACTION
    };

    class MatrixEvent {
    public:
        virtual ~MatrixEvent() = default;
    };

    struct Message {
        std::shared_ptr<UserInfo> user;
        std::string event_id;
        std::string body;
        std::string url;
        std::string thumbnail_url;
        std::string related_event_id;
        sf::Vector2i thumbnail_size; // Set to {0, 0} if not specified
        RelatedEventType related_event_type = RelatedEventType::NONE;
        bool notification_mentions_me = false;
        bool cache = false;
        std::string transaction_id;
        time_t timestamp = 0; // In milliseconds
        MessageType type;
        std::shared_ptr<Message> replaced_by = nullptr;
        // TODO: Store body item ref here
    };

    struct RoomData {
        std::shared_ptr<UserInfo> get_user_by_id(const std::string &user_id);
        void add_user(std::shared_ptr<UserInfo> user);

        void set_user_read_marker(std::shared_ptr<UserInfo> &user, const std::string &event_id);
        std::string get_user_read_marker(const std::shared_ptr<UserInfo> &user);

        std::string get_user_display_name(const std::shared_ptr<UserInfo> &user);
        std::string get_user_avatar_url(const std::shared_ptr<UserInfo> &user);

        // Set to empty to remove (in which case the display name will be the user id)
        void set_user_display_name(std::shared_ptr<UserInfo> &user, std::string display_name);
        void set_user_avatar_url(std::shared_ptr<UserInfo> &user, std::string avatar_url);

        // Ignores duplicates, returns the number of added messages
        size_t prepend_messages_reverse(const std::vector<std::shared_ptr<Message>> &new_messages);
        // Ignores duplicates, returns the number of added messages
        size_t append_messages(const std::vector<std::shared_ptr<Message>> &new_messages);

        std::shared_ptr<Message> get_message_by_id(const std::string &id);

        std::vector<std::shared_ptr<UserInfo>> get_users_excluding_me(const std::string &my_user_id);

        void acquire_room_lock();
        void release_room_lock();

        const std::vector<std::shared_ptr<Message>>& get_messages_thread_unsafe() const;
        const std::vector<std::string>& get_pinned_events_unsafe() const;

        bool has_prev_batch();
        void set_prev_batch(const std::string &new_prev_batch);
        std::string get_prev_batch();

        bool has_name();
        void set_name(const std::string &new_name);
        // TODO: Remove this
        std::string get_name();

        bool has_avatar_url();
        void set_avatar_url(const std::string &new_avatar_url);
        std::string get_avatar_url();

        void set_pinned_events(std::vector<std::string> new_pinned_events);
        std::set<std::string>& get_tags_unsafe();

        void clear_data();

        std::string id;

        // These 5 variables are set by QuickMedia, not the matrix plugin
        bool initial_prev_messages_fetch = true;
        bool last_message_read = true;
        bool users_fetched = false;
        time_t last_read_message_timestamp = 0;
        std::shared_ptr<BodyItem> body_item;

        // These are messages fetched with |Matrix::get_message_by_id|. Needed to show replies, when replying to old message not part of /sync.
        // The value is nullptr if the message is fetched and cached but the event if referenced an invalid message.
        // TODO: Verify if replied to messages are also part of /sync; then this is not needed.
        std::unordered_map<std::string, std::shared_ptr<Message>> fetched_messages_by_event_id;

        size_t messages_read_index = 0;
        bool pinned_events_updated = false;
        bool name_is_fallback = false;
        bool avatar_is_fallback = false;
        std::atomic_int64_t last_message_timestamp = 0;

        std::atomic_int unread_notification_count = 0;
        std::atomic_int64_t read_marker_event_timestamp = 0;

        size_t index = 0;
    private:
        std::recursive_mutex user_mutex;
        std::recursive_mutex room_mutex;

        std::string name;
        std::string avatar_url;
        std::string prev_batch;

        // Each room has its own list of user data, even if multiple rooms has the same user
        // because users can have different display names and avatars in different rooms.
        std::unordered_map<std::string, std::shared_ptr<UserInfo>> user_info_by_user_id;
        std::vector<std::shared_ptr<Message>> messages;
        std::unordered_map<std::string, std::shared_ptr<Message>> message_by_event_id;
        std::vector<std::string> pinned_events;
        std::set<std::string> tags;
    };

    struct Invite {
        std::string room_name;
        std::string room_avatar_url;
        std::shared_ptr<UserInfo> invited_by;
        time_t timestamp = 0; // In milliseconds
        bool new_invite = false;
    };

    enum class MessageDirection {
        BEFORE,
        AFTER
    };

    struct UploadInfo {
        ContentType content_type;
        size_t file_size;
        std::optional<Dimensions> dimensions;
        std::optional<double> duration_seconds;
        std::string content_uri;
    };

    using Messages = std::vector<std::shared_ptr<Message>>;

    struct SyncData {
        Messages messages;
        std::optional<std::vector<std::string>> pinned_events;
    };

    using Rooms = std::vector<RoomData*>;

    bool message_contains_user_mention(const std::string &msg, const std::string &username);
    bool message_is_timeline(Message *message);
    void body_set_selected_item(Body *body, BodyItem *selected_item);
    std::string create_transaction_id();

    enum class MatrixPageType {
        ROOM_LIST,
        CHAT
    };

    enum class LeaveType {
        LEAVE,
        KICKED,
        BANNED
    };

    class MatrixDelegate {
    public:
        virtual ~MatrixDelegate() = default;

        virtual void join_room(RoomData *room) = 0;
        virtual void leave_room(RoomData *room, LeaveType leave_type, const std::string &reason) = 0;

        // Note: calling |room| methods inside this function is not allowed
        virtual void room_add_tag(RoomData *room, const std::string &tag) = 0;
        // Note: calling |room| methods inside this function is not allowed
        virtual void room_remove_tag(RoomData *room, const std::string &tag) = 0;
        virtual void room_add_new_messages(RoomData *room, const Messages &messages, bool is_initial_sync, bool sync_is_cache, MessageDirection message_dir) = 0;

        virtual void add_invite(const std::string &room_id, const Invite &invite) = 0;
        virtual void remove_invite(const std::string &room_id) = 0;

        virtual void add_unread_notification(RoomData *room, std::string event_id, std::string sender, std::string body) = 0;

        virtual void update(MatrixPageType page_type) { (void)page_type; }

        virtual void clear_data() = 0;
    };

    class Matrix;
    class MatrixRoomsPage;
    class MatrixRoomTagsPage;
    class MatrixInvitesPage;
    class MatrixChatPage;

    class MatrixQuickMedia : public MatrixDelegate {
    public:
        MatrixQuickMedia(Program *program, Matrix *matrix, MatrixRoomsPage *rooms_page, MatrixRoomTagsPage *room_tags_page, MatrixInvitesPage *invites_page);

        void join_room(RoomData *room) override;
        void leave_room(RoomData *room, LeaveType leave_type, const std::string &reason) override;
        void room_add_tag(RoomData *room, const std::string &tag) override;
        void room_remove_tag(RoomData *room, const std::string &tag) override;
        void room_add_new_messages(RoomData *room, const Messages &messages, bool is_initial_sync, bool sync_is_cache, MessageDirection message_dir) override;

        void add_invite(const std::string &room_id, const Invite &invite) override;
        void remove_invite(const std::string &room_id) override;

        void add_unread_notification(RoomData *room, std::string event_id, std::string sender, std::string body) override;

        void update(MatrixPageType page_type) override;

        void clear_data() override;

        Program *program;
        Matrix *matrix;
        MatrixRoomsPage *rooms_page;
        MatrixRoomTagsPage *room_tags_page;
        MatrixInvitesPage *invites_page;
    private:
        void update_room_description(RoomData *room, Messages &new_messages, bool is_initial_sync, bool sync_is_cache);
        void update_pending_room_messages(MatrixPageType page_type);
    private:
        struct RoomMessagesData {
            Messages messages;
            bool is_initial_sync;
            bool sync_is_cache;
            MessageDirection message_dir;
        };

        struct Notification {
            std::string event_id;
            std::string sender;
            std::string body;
        };

        std::map<RoomData*, std::shared_ptr<BodyItem>> room_body_item_by_room;
        std::mutex room_body_items_mutex;
        std::map<RoomData*, RoomMessagesData> pending_room_messages;
        std::mutex pending_room_messages_mutex;

        std::unordered_map<RoomData*, std::vector<Notification>> unread_notifications;
        std::map<RoomData*, std::shared_ptr<Message>> last_message_by_room;
    };

    class MatrixRoomsPage : public Page {
    public:
        MatrixRoomsPage(Program *program, Body *body, std::string title, MatrixRoomTagsPage *room_tags_page, SearchBar *search_bar);
        ~MatrixRoomsPage() override;

        const char* get_title() const override { return title.c_str(); }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        bool clear_search_after_submit() override { return true; }

        void on_navigate_to_page(Body *body) override;

        void update() override;
        void add_body_item(std::shared_ptr<BodyItem> body_item);

        void move_room_to_top(RoomData *room);
        void remove_body_item_by_room_id(const std::string &room_id);

        void set_current_chat_page(MatrixChatPage *chat_page);

        void clear_data();
        void sort_rooms();

        MatrixQuickMedia *matrix_delegate = nullptr;
        bool filter_on_update = false;
    private:
        std::mutex mutex;
        std::vector<std::shared_ptr<BodyItem>> room_body_items;
        std::vector<std::string> pending_remove_body_items;
        Body *body = nullptr;
        std::string title;
        MatrixRoomTagsPage *room_tags_page = nullptr;
        SearchBar *search_bar = nullptr;
        MatrixChatPage *current_chat_page = nullptr;
        bool clear_data_on_update = false;
        bool sort_on_update = false;
    };

    class MatrixRoomTagsPage : public Page {
    public:
        MatrixRoomTagsPage(Program *program, Body *body, SearchBar *search_bar) : Page(program), body(body), search_bar(search_bar) {}
        const char* get_title() const override { return "Tags"; }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        bool clear_search_after_submit() override { return true; }

        void update() override;
        void add_room_body_item_to_tag(std::shared_ptr<BodyItem> body_item, const std::string &tag);
        void remove_room_body_item_from_tag(std::shared_ptr<BodyItem> body_item, const std::string &tag);

        void move_room_to_top(RoomData *room);
        void remove_body_item_by_room_id(const std::string &room_id);

        void set_current_rooms_page(MatrixRoomsPage *rooms_page);

        void clear_data();
        void sort_rooms();

        MatrixQuickMedia *matrix_delegate = nullptr;
        bool filter_on_update = false;
    private:
        struct TagData {
            std::shared_ptr<BodyItem> tag_item;
            std::vector<std::shared_ptr<BodyItem>> room_body_items;
        };

        std::recursive_mutex mutex;
        Body *body;
        std::map<std::string, TagData> tag_body_items_by_name;
        std::map<std::string, std::vector<std::shared_ptr<BodyItem>>> add_room_body_items_by_tags;
        std::map<std::string, std::vector<std::shared_ptr<BodyItem>>> remove_room_body_items_by_tags;
        MatrixRoomsPage *current_rooms_page = nullptr;
        bool clear_data_on_update = false;
        SearchBar *search_bar = nullptr;
    };

    class MatrixInvitesPage : public Page {
    public:
        MatrixInvitesPage(Program *program, Matrix *matrix, Body *body, SearchBar *search_bar);

        const char* get_title() const override { return title.c_str(); }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        bool clear_search_after_submit() override { return true; }

        void update() override;
        void add_body_item(std::shared_ptr<BodyItem> body_item);
        void remove_body_item_by_room_id(const std::string &room_id);

        void clear_data();
        bool filter_on_update = false;
    private:
        Matrix *matrix;
        std::mutex mutex;
        std::vector<std::shared_ptr<BodyItem>> body_items;
        std::vector<std::string> pending_remove_body_items;
        Body *body;
        std::string title = "Invites (0)";
        size_t prev_invite_count = 0;
        bool clear_data_on_update = false;
        SearchBar *search_bar = nullptr;
    };

    class MatrixInviteDetailsPage : public Page {
    public:
        MatrixInviteDetailsPage(Program *program, Matrix *matrix, MatrixInvitesPage *invites_page, std::string room_id, std::string title) :
            Page(program), matrix(matrix), invites_page(invites_page), room_id(std::move(room_id)), title(std::move(title)) {}
        const char* get_title() const override { return title.c_str(); }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;

        Matrix *matrix;
        MatrixInvitesPage *invites_page;
        const std::string room_id;
        const std::string title;
    };

    // Dummy, only play one video. TODO: Play all videos in room, as related videos?
    class MatrixVideoPage : public VideoPage {
    public:
        MatrixVideoPage(Program *program) : VideoPage(program) {}
        const char* get_title() const override { return ""; }
        std::unique_ptr<RelatedVideosPage> create_related_videos_page(Program*, const std::string&, const std::string&) override {
            return nullptr;
        }
        std::unique_ptr<LazyFetchPage> create_channels_page(Program*, const std::string&) override {
            return nullptr;
        }
        std::string get_url() override { return url; }
        std::string url;
    };

    class MatrixChatPage : public Page {
    public:
        MatrixChatPage(Program *program, std::string room_id, MatrixRoomsPage *rooms_page);
        ~MatrixChatPage();

        const char* get_title() const override { return ""; }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override {
            (void)title;
            (void)url;
            (void)result_tabs;
            return PluginResult::ERR;
        }
        PageTypez get_type() const override { return PageTypez::CHAT; }
        void update() override;

        const std::string room_id;
        MatrixQuickMedia *matrix_delegate = nullptr;
        MatrixRoomsPage *rooms_page = nullptr;
        bool should_clear_data = false;
    };

    class MatrixRoomDirectoryPage : public Page {
    public:
        MatrixRoomDirectoryPage(Program *program, Matrix *matrix) : Page(program), matrix(matrix) {}
        const char* get_title() const override { return "Room directory"; }
        bool allow_submit_no_selection() const override { return true; }
        bool clear_search_after_submit() override { return true; }
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    private:
        Matrix *matrix;
    };

    class MatrixServerRoomListPage : public LazyFetchPage {
    public:
        MatrixServerRoomListPage(Program *program, Matrix *matrix, const std::string &server_name) : LazyFetchPage(program), matrix(matrix), server_name(server_name), current_page(0) {}
        const char* get_title() const override { return "Select a room to join"; }
        bool search_is_filter() override { return false; }
        PluginResult lazy_fetch(BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    private:
        Matrix *matrix;
        const std::string server_name;
        std::string next_batch;
        std::string search_term;
        int current_page;
    };

    class Matrix {
    public:
        void start_sync(MatrixDelegate *delegate, bool &cached);
        void stop_sync();
        bool is_initial_sync_finished() const;
        // Returns true if initial sync failed, and |err_msg| is set to the error reason in that case
        bool did_initial_sync_fail(std::string &err_msg);
        void get_room_sync_data(RoomData *room, SyncData &sync_data);

        void get_all_synced_room_messages(RoomData *room, Messages &messages);
        void get_all_pinned_events(RoomData *room, std::vector<std::string> &events);
        PluginResult get_previous_room_messages(RoomData *room, Messages &messages, bool latest_messages = false);

        // |url| should only be set when uploading media.
        // TODO: Make api better.
        PluginResult post_message(RoomData *room, const std::string &body, std::string &event_id_response, const std::optional<UploadInfo> &file_info, const std::optional<UploadInfo> &thumbnail_info, const std::string &msgtype = "");
        // |relates_to| is from |BodyItem.userdata| and is of type |Message*|
        // If |custom_transaction_id| is empty, then a new transaction id is generated
        PluginResult post_reply(RoomData *room, const std::string &body, void *relates_to, std::string &event_id_response, const std::string &custom_transaction_id = "");
        // |relates_to| is from |BodyItem.userdata| and is of type |Message*|
        PluginResult post_edit(RoomData *room, const std::string &body, void *relates_to, std::string &event_id_response);
        // |relates_to| is from |BodyItem.userdata| and is of type |Message*|
        PluginResult post_reaction(RoomData *room, const std::string &body, void *relates_to, std::string &event_id_response);
        
        PluginResult post_file(RoomData *room, const std::string &filepath, std::string &event_id_response, std::string &err_msg);
        PluginResult login(const std::string &username, const std::string &password, const std::string &homeserver, std::string &err_msg);
        PluginResult logout();

        // |message| is from |BodyItem.userdata| and is of type |Message*|
        PluginResult delete_message(RoomData *room, void *message, std::string &err_msg);

        PluginResult load_cached_session();

        PluginResult on_start_typing(RoomData *room);
        PluginResult on_stop_typing(RoomData *room);

        PluginResult set_read_marker(RoomData *room, const std::string &event_id, int64_t event_timestamp);

        PluginResult join_room(const std::string &room_id);
        PluginResult leave_room(const std::string &room_id);

        // If |since| is empty, then the first page is fetched
        PluginResult get_public_rooms(const std::string &server, const std::string &search_term, const std::string &since, BodyItems &rooms, std::string &next_batch);

        // |message| is from |BodyItem.userdata| and is of type |Message*|
        bool was_message_posted_by_me(void *message);

        std::string message_get_author_displayname(Message *message) const;

        // Cached
        PluginResult get_config(int *upload_size);

        std::shared_ptr<UserInfo> get_me(RoomData *room);

        const std::string& get_homeserver_domain() const;

        // Returns nullptr if message cant be found. Note: cached
        std::shared_ptr<Message> get_message_by_id(RoomData *room, const std::string &event_id);

        RoomData* get_room_by_id(const std::string &id);
        void update_user_with_latest_state(RoomData *room, const std::string &user_id);
        void update_room_users(RoomData *room);
    private:
        PluginResult set_qm_last_read_message_timestamp(RoomData *room, int64_t timestamp);

        PluginResult parse_sync_response(const rapidjson::Document &root, bool is_additional_messages_sync, bool initial_sync);
        PluginResult parse_notifications(const rapidjson::Value &notifications_json);
        PluginResult parse_sync_account_data(const rapidjson::Value &account_data_json, std::optional<std::set<std::string>> &dm_rooms);
        PluginResult parse_sync_room_data(const rapidjson::Value &rooms_json, bool is_additional_messages_sync, bool initial_sync);
        PluginResult get_previous_room_messages(RoomData *room_data, bool latest_messages, size_t &num_new_messages);
        void events_add_user_info(const rapidjson::Value &events_json, RoomData *room_data);
        std::shared_ptr<UserInfo> parse_user_info(const rapidjson::Value &json, const std::string &user_id, RoomData *room_data);
        void events_set_user_read_marker(const rapidjson::Value &events_json, RoomData *room_data, std::shared_ptr<UserInfo> &me);
        // Returns the number of messages added
        size_t events_add_messages(const rapidjson::Value &events_json, RoomData *room_data, MessageDirection message_dir, bool has_unread_notifications);
        void events_set_room_name(const rapidjson::Value &events_json, RoomData *room_data);
        void set_room_name_to_users_if_empty(RoomData *room, const std::string &room_creator_user_id);
        void events_add_pinned_events(const rapidjson::Value &events_json, RoomData *room_data);
        void events_add_room_to_tags(const rapidjson::Value &events_json, RoomData *room_data);
        void add_invites(const rapidjson::Value &invite_json);
        void remove_rooms(const rapidjson::Value &leave_json);
        std::shared_ptr<Message> parse_message_event(const rapidjson::Value &event_item_json, RoomData *room_data);
        PluginResult upload_file(RoomData *room, const std::string &filepath, UploadInfo &file_info, UploadInfo &thumbnail_info, std::string &err_msg, bool upload_thumbnail = true);
        void add_room(std::unique_ptr<RoomData> room);
        void remove_room(const std::string &room_id);
        // Returns false if an invite to the room already exists
        bool set_invite(const std::string &room_id, Invite invite);
        // Returns true if an invite for |room_id| exists
        bool remove_invite(const std::string &room_id);
        void set_next_batch(std::string new_next_batch);
        std::string get_next_batch();
        void clear_sync_cache_for_new_sync();
        std::shared_ptr<UserInfo> get_user_by_id(RoomData *room, const std::string &user_id);
        std::string get_filter_cached();
    private:
        std::vector<std::unique_ptr<RoomData>> rooms;
        std::unordered_map<std::string, size_t> room_data_by_id; // value is an index into |rooms|
        std::recursive_mutex room_data_mutex;
        std::string my_user_id;
        std::string access_token;
        std::string homeserver;
        std::string homeserver_domain;
        std::optional<int> upload_limit;
        std::string next_batch;
        std::mutex next_batch_mutex;

        std::unordered_map<std::string, Invite> invites;
        std::mutex invite_mutex;

        std::thread sync_thread;
        std::thread sync_additional_messages_thread;
        std::thread notification_thread;
        MessageQueue<bool> additional_messages_queue;
        bool sync_running = false;
        bool sync_failed = false;
        bool sync_is_cache = false;
        std::string sync_fail_reason;
        MatrixDelegate *delegate = nullptr;
        std::optional<std::string> filter_cached;

        std::unordered_set<std::string> my_events_transaction_ids;
    };
}