#pragma once

#include "Page.hpp"

namespace QuickMedia {
    enum class PostResult {
        OK,
        TRY_AGAIN,
        BANNED,
        ERR
    };

    class ImageBoardThreadPage : public VideoPage {
    public:
        ImageBoardThreadPage(Program *program, std::string board_id, std::string thread_id, std::vector<std::string> cached_media_urls) : VideoPage(program), board_id(std::move(board_id)), thread_id(std::move(thread_id)), cached_media_urls(std::move(cached_media_urls)) {}
        const char* get_title() const override { return ""; }
        PageTypez get_type() const override { return PageTypez::IMAGE_BOARD_THREAD; }

        BodyItems get_related_media(const std::string &url, std::string &channel_url) override;
        std::unique_ptr<RelatedVideosPage> create_related_videos_page(Program *program, const std::string &video_url, const std::string &video_title) override;
        std::unique_ptr<LazyFetchPage> create_channels_page(Program*, const std::string&) override {
            return nullptr;
        }
        std::string get_url() override { return video_url; }
        virtual PluginResult login(const std::string &token, const std::string &pin, std::string &response_msg);
        virtual PostResult post_comment(const std::string &captcha_id, const std::string &comment) = 0;
        virtual const std::string& get_pass_id();

        const std::string board_id;
        const std::string thread_id;
        const std::vector<std::string> cached_media_urls;
        std::string video_url;
    };
}