#pragma once

#include "Manga.hpp"
#include <functional>

namespace QuickMedia {
    class MangadexSearchPage : public Page {
    public:
        MangadexSearchPage(Program *program) : Page(program) {}
        const char* get_title() const override { return "All"; }
        bool search_is_filter() override { return false; }
        SearchResult search(const std::string &str, BodyItems &result_items) override;
        PluginResult get_page(const std::string &str, int page, BodyItems &result_items) override;
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
        sf::Vector2i get_thumbnail_max_size() override { return sf::Vector2i(101, 141); };
    private:
        SearchResult search(const std::string &str, int page, BodyItems &result_items);
        bool get_rememberme_token(std::string &rememberme_token);
        std::optional<std::string> rememberme_token;
    };

    class MangadexChaptersPage : public MangaChaptersPage {
    public:
        MangadexChaptersPage(Program *program, std::string manga_name, std::string manga_url) : MangaChaptersPage(program, std::move(manga_name), std::move(manga_url)) {}
        PluginResult submit(const std::string &title, const std::string &url, std::vector<Tab> &result_tabs) override;
    protected:
        bool extract_id_from_url(const std::string &url, std::string &manga_id) const override;
        const char* get_service_name() const override { return "mangadex"; }
    };

    class MangadexImagesPage : public MangaImagesPage {
    public:
        MangadexImagesPage(Program *program, std::string manga_name, std::string chapter_name, std::string url) : MangaImagesPage(program, std::move(manga_name), std::move(chapter_name), std::move(url)) {}

        ImageResult get_number_of_images(int &num_images) override;
        ImageResult for_each_page_in_chapter(PageCallback callback) override;

        const char* get_service_name() const override { return "mangadex"; }
    private:
        // Cached
        ImageResult get_image_urls_for_chapter(const std::string &url);
        bool save_mangadex_cookies(const std::string &url, const std::string &cookie_filepath);
    };
}