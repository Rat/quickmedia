#include <stdio.h>
#include "../include/NetUtils.hpp"

#define assert_fail(str) do { fprintf(stderr, "Assert failed on line %d, reason: %s\n", __LINE__, (str)); exit(1); } while(0)
#define assert_equals(a, b) do { if((a) != (b)) { fprintf(stderr, "Assert failed on line %d, %s == %s\n", __LINE__, #a, #b); exit(1); } } while(0)

int main() {
    std::vector<std::string> urls;
    const char *str;

    str = "example.com";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "example.com, is where I like to go";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "The website I like to go to is example.com";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "example.com. Is also a website";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "example.com: the best test website";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "example.com");

    str = "these. are. not. websites.";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 0);

    str = "This is not an url: example.";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 0);

    str = "the.se/~#423-_/2f.no/3df a.re considered sub.websit.es, this.is.not";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 3);
    assert_equals(urls[0], "the.se/~#423-_/2f.no/3df");
    assert_equals(urls[1], "a.re");
    assert_equals(urls[2], "sub.websit.es");

    str = "(see https://emojipedia.org/emoji/%23%EF%B8%8F%E2%83%A3/)";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "https://emojipedia.org/emoji/%23%EF%B8%8F%E2%83%A3/");

    str = "[sneed](https://sneedville.com)";
    urls = QuickMedia::ranges_get_strings(str, QuickMedia::extract_urls(str));
    assert_equals(urls.size(), 1);
    assert_equals(urls[0], "https://sneedville.com");
    return 0;
}
