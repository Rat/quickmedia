Give user the option to start where they left off or from the start or from the start (for manga).
Add grid-view when thumbnails are visible.
Add scrollbar.
Somehow deal with youtube banning ip when searching too often.
Optimize shadow rendering for items (Right now they fill too much space that is behind items). It should also be a blurry shadow.
When continuing to read manga from a different page from the first and there is no cache for the chapter, then start downloading from the current page instead of page 1.
Show progress of manga in the history tab (current chapter out of total chapters).
Animate page navigation.
Add greentext support for 4chan quotes.
Add support for special formatting for posts by admins on imageboards.
For image boards, track (You)'s and show notification when somebody replies to your post.
Go to next chapter when reaching the end of the chapter in image endless mode.
Some text is not visible on 4chan, such as code blocks.
Add login page for mangadex instead of having to manually add remember_me token to config file.
Allow deleting watch history with delete key (and show confirmation).
Add navigation to nyaa.si submitter torrents.
Create a large texture and add downloaded images to it. This will save memory usage because sfml has to use power of two textures (and so does opengl internally) for textures, so if you have multiple textures they will use more memory than one large texture with the same texture data.
Use fallback cjk font for regular sf::Text as well (search, tabs, chapter name when viewing a page, path in file-manager, etc).
Fix some japanese fonts not rendering (half width alphanumeric?).
Extract thumbnail from images that are being downloaded, while its downloading and show that while the full image is downloading (upscaled, or with blurhash).
Add setting to disable sending typing events to the server (matrix).
Support emoji (mainly for matrix), by readding Text code from dchat. Also do the same but for inline images, text editing and url colors and clicking (also clicking on inline images).
Also take code from dchat to support gifs (inline in text).
Use pixel buffer object for asynchronous texture transfer to gpu? is this necessary?
Add option to edit input in vim (using temporary file).
Scrolling in images still messes up the |current| page sometimes, need a way to fix this.
Add ctrl+i keybind when viewing an image on 4chan to reverse image search it (using google, yandex and saucenao).
Show filename at the bottom when viewing an image/video on 4chan.
Add ctrl+c keybinding to copy the url of the previewing image.
Add ctrl+c keybiding to copy the url to the currently selected post on 4chan.
Add ctrl+s to save the previewing image/video (for images that would be a copy, since its already stored in cache and for videos youtube-dl would be used).
Use https://github.com/simdjson/simdjson as a json library in other parts than matrix.
Sanitize check: do not allow pasting more than 2gb of text.
Only add related videos to recommendations if its the first time we watch the video. This is to prevent rewatching a video multiple times from messing up recommendations.
Implement mentions in matrix with an autofill list, like on element. Also do the same with / commands.
Add option to disable autosearch and search when pressing enter instead or something? this would be needed for mobile phones where typing is slow.
Sleep when idle, to reduce cpu usage from 1-2% to 0%, important for mobile devices. Also render view to a rendertexture and render that instead of redrawing every time every time.
Provide a way to specify when notifications should be received (using matrix api) and also read the notification config from matrix. Also provide a way to disable notifications globally.
Use quickmedia to show image in matrix rooms, instead of mpv.
Add command to ban users.
Support peertube (works with mpv, but need to implement search and related videos).
Scroll to bottom when receiving a new message even if the selected message is not the last one. It should instead scroll if the last message is visible on the screen.
Also add a tab for common directories and recently accessed files/directories (the directories would be the directory of used files).
Provide a way to go to the first unread message in matrix and also show a marker in the body (maybe a red line?) where the first unread message is.
Cleanup keybindings. Some require ctrl, some dont.
Add room topic beside room name in matrix (in messages tab).
Add /me to matrix, emoji, reactions...
Set the icon of the window to be the icon of the plugin. Nice for KDE, GNOME, etc with titlebars.
If --no-audio is used then music should be played with a lightweight music player instead. MPV is heavy even for music (60mb RAM). Maybe use sfml audio functions?
Optimize startup time.
Update 4chan thread in real time, just like 4chan-x.
Save the original event message, so when replying for example we can use the original message as the replying to message, rather than our converted "body" text.
Remove tidy dependency and use my own html-parser.
Add option to sort by other than timestamp for nyaa.si.
Add url preview for matrix (using matrix api, fallback to client url preview (using our own url preview project) if disabled by the homeserver).
IMPORTANT: Cleanup old messages in matrix (from matrix plugin), and instead either save them to disk or refetch them from server when going up to read old messages. (High memory usage, high disk space)
Use memberName() instead of key() when iterating json object. key() creates a copy, memberName() doesn't.
Do not try to reload/redownload thumbnail that fails to download after its cleared when its no longer visible on screen and then becomes visible.
Show google recaptcha on youtube when search/play fails, which can happen when using tor.
Show notifications when we receive a message in a matrix room even if we are not mentioned. This happens when we have set to receive notifications for all messages.
If there are multiple users with the same name in a matrix room, then display the user id beside the displayname.
Show 4chan warnings as warnings instead of ban when posting a message (show the warning message) and then post the message.
Add tabs. Using tabs with tabbed is not as good of a solution as it would use much more memory (opengl context cost) and with our own tabs, we can clear thumbnails and other cache when a tab is in the background. Changing tab either with ctrl+tab or mouse click. ctrl+enter to open a new tab. Ctrl+q or ctrl+w to close a tab.
Remove related videos that have already been watched (except the first related video, which is the "watch next" video, usually the next part of a serie).
Add F5 to refresh page.
Support m.sticker, m.direct, and other matrix events.
Allow choosing which translation/scanlation to use on mangadex. Right now it uses the latest one, which is most likely to be the best.
Add file upload to 4chan.
Retry download if it fails, at least 3 times (observed to be needed for mangadex images).
Readd autocomplete, but make it better with a proper list. Also readd 4chan login page.
Modify sfml to use GL_COMPRESSED_LUMINANCE and other texture compression modes (in sf::Texture). This reduces memory usage by half.
Decrease memory usage even further (mostly in matrix /sync when part of large rooms) by using rapidjson SAX style API to stream json string into SAX style parsing.
Sometimes we fail to get images in mangadex, most common reason being that the manga is licensed and we can't view the manga on mangadex. QuickMedia should implement mangaplus and redirect us to mangaplus plugin to view the manga, or simply show that we cant view the manga because its licensed.
Show redacted messages even when part of the initial sync in matrix. Right now they are hidden while new sync redacted messages are not.
Fix inconsistent behavior when editing a message that is replied to in matrix. Right now if the replied to message already exits in the body then its used directly and when editing that message the reply message shows the edit embedded, but not if the edit is of an body item that is created because we dont already have it,
to fix this we could perhaps replace the newly created body items for replies when loading old messages and one of the old messages is also one of the embedded messages (by event id).
Add button to skip to next video. MPV has this feature when setting "next" video (can be done over IPC).
Use a custom allocator that replaces malloc/realloc/free/new/delete to release memory properly, using munmap in free/delete. The C allocator doesn't do that! memory usage remains high after one large allocation. The C allocator only marks it as free.
Ignore timestamp ordering for messages in matrix? element seems to do that (or only for new messages???), and also we need the latest message to be last I guess to set read markers properly?
Merge |Page::search| and |Page::get_page|. get_page with page 0 should be the same as search.
Disable posting in 4chan thread if closed (thread json contains "closed" field for OP).
Remove calls to get the original message of an edit in edits and replies in matrix if possible. These calls take additional time, and with a slow homeserver or high ping this could make messages to be delayed by an annoying amount of time.
Read image exif into to apply image rotation. This is common in images taken on phones. If not done, the width and height will also be mixed and thumbnail fallback size will be incorrectly calculated (for example in matrix).
Handle M_LIMIT_EXCEEDED in matrix
Maybe dont clear cache for body items when filtering.
Change scroll in body when previous items change size (such as when thumbnail has finished loading).
Pressing enter on a pinned message should go to the message in the messages tab.
Display file list for nyaa.
Remove reply formatting for NOTICE in matrix as well.
Scroll body when adding new items and the selected item fits after the scroll (needed for matrix where we want to see new messages when the last item is not selected). Or show the last item when its not visible in matrix (at the bottom, just like when replying/editing).
Implement our own encryption for matrix. This is also needed to make forwarded message work. Pantalaimon ignores them!
Modify matrix sync to download and parse json but not handle it, and then add a function to handle the json. This would allow us to remove all the mutex code if we would call that new method from the main thread (similar to chromium multithreading).
Fetch replies/pinned message using multiple threads.
Show in room tags list when there is a message in any of the rooms in the tag.
Support webp. Then switch to the youtube thumbnails from the response json instead of hqdefault, to remove the black bars.
Show images while they download by showing them as scanlines starting from the top. Needed for slow websites such as 4chan.
Use curl parallel download instead of downloading with multiple threads.
Handle matrix groups? (which also contains join, invite, leave...).
Add functionality to ignore users in matrix. This is done with an ignore request and we wont get messages and invites from that user anymore. Also add option to ignore in the invites page.
Add keybind to go to invites page from any page.
Show marker beside pinned messages tab name if there are new pinned messages.
Make /logout work everywhere, not only in room message input.
Add a notifications tab to show messages that mention us in all rooms (and then press enter to go to that message in that room), also add a unread/mentioned rooms list tab to only show rooms with unread messages or mentions.
Disable message input in matrix when muted.
Preview rooms?
Handle matrix token being invalidated while running.
Update upload limit if its updated on the server (can it be updated while the server is running?).
Editing a reply removes reply formatting (both in body and formatted_body). Element also does this when you edit a reply twice. This breaks element mobile that is unable to display replied-to messages without correct formatting (doesn't fetch the replied-to message).
This also removes the mentioned name which breaks mention for reply.
Implement m.room.tombstone.
Show a marker when a room uses encryption.
Remove replied-to message text in room preview. That shows ignored users text and we want to see the reply message instead anyways.
Update room name/avatar with new data in /sync.
Scroll tabs if there are more than 3 tab items and show arrow on left/right side when there are more items to see.
Make a shader for Text for changing color instead of updating the text geometry. Or loop vertices and set their color to the new color without updating the text geometry.
Automatically retry sending messages that fails to send (after timeout). These failed to send messages should be stored on disk and retried when going back to the room or restarting QuickMedia.
Also make message deletion provisional (make it gray while its deleting the message).
Continue matrix requests when switching room, instead of resetting them when going from chat page to room list page (such as post message request).
Improve /sync by not removing cached data on initial sync, and also always append data to sync file instead of overwriting sync file on "initial sync". Also cache "since", but take into consideration that not all messages are fetched on the initial sync,
then add a gap between old messages from before sync and after sync so we can fetch the messages between the old messages and new messages and remove the gap when the fetched messages contains any of the old messages. After the sync, ignored users messages should be removed from the cache and messages list. Also take into consideration unignoring users.
Fetching of previous messages should also be saved in the /sync file and messages fetched with get_message_by_id, which would cache embedded items and pinned messages; also cache users.
If manga page fails to download then show "failed to download image" as text and bind F5 to refresh (retry download).
Use <img src to add custom emojis, and add setting for adding/removing custom emoji.
Use window title when room name changes in matrix.
Prev token is incorrect if additional messages have been fetched and comparing to get_previous_messages set token, because prev token is not set when fetching additional messages.
Create multiple BodyItem types. BodyItem has a lot of fields and most of them are not always used.
Have a list of redacted events so when fetching previous events, we can filter out the redacted events (directly in the matrix plugin).
Add grid view to matrix and navigate between them using alt+arrow keys.
Remove display names from reactions if there are many reactions, and instead group them into: reaction (#number of this type of reaction); for example: 👍 2.
Make reaction and deleted message provisional.
Allow removing reactions.
When fetching previous messages in matrix, fetching until there are 0 messages or until there is at least 1 visible item. This is needed because right now we could fetch 10 messages, all which are delete/edit/react and it will look like there are no more messages to fetch to the user.
QuickMedia crashes if you enter a room that you have left before. This could happen if cache is loaded that has a room that is then getting removed when sync is finished.
Removing a reaction should be shown as "removed reaction: <reaction text>" in the room description instead of "Message deleted".
Limit size of Entry and scroll content instead.
Have an option to remove membership events from room unread messages, so that only text/media messages update room unread description in the room list. This could be implemented by doing /sync and filtering to only show m.room.message event types or something similar and limiting it to the last message,
then comparing that to the read marker.
Comparing latest message for unread message should be done by comparing event id ascii-wise instead of checking if the latest message is equal to the read marker event id. This is to fix unread messages in the case of the latest message in a room being deleted.
Support replying to messages with media, by typing /upload in the reply text.
Instead of doing a GET for the first N bytes to check if a video is streamable, start streaming the video and if the first bytes doesn't contain MOOV then wait until the whole video has downloaded before playing it.
Create thumbnail when uploading an image in matrix.
If a message reply is edited and its a reply to us, then we want the text to be red even if its edited.
Implement matrix spoiler, see: https://github.com/matrix-org/matrix-doc/blob/master/proposals/2010-spoilers.md.
Make the messages that mention us red using the matrix notification api that we already use. Mark those messages as mentions us.
Replace sfml font glyph loading completely with FreeType. There is a very old bug in sfml that causes text to get corrupt sometimes. Im guessing this happens when adding new characters to the font atlas and that coordinates for the glyphs become incorrect?
Add arguments to pipe plugin to pass input and output fifo for sending commands to QuickMedia and receiving events.
Update thumbnails in file-manager if an image is replaced, by including the modify date of the image in the thumbnail cache as well.
Create a workaround for dwm terminal swallow patch stealing mpv when moving QuickMedia to another monitor sometimes. Maybe check for structure notify events on mpv and reparent and select input on the mpv window again?
Add option to decline and mute user in invites. This is to combat invite spam, where muted users cant invite you.
Allow hiding videos so they dont show up in recommendations and related videos.
Add an option to select video resolution, if we want to use less power and less bandwidth for example.
Use mpv option --gpu-context=x11egl on pinephone to force xwayland on wayland, to be able to embed the mpv window inside the quickmedia.
Replies to the local user shouldn't remove the red text. Maybe fix this by checking if the reply to message user is the local user or when the replied to message has loaded then make the reply red if its a reply to us. Also for existing messages check if the message is a notification message and then make the message red.
Sort reactions by timestamp.
Check what happens with xsrf_token if comments are not fetched for a long time. Does it time out? if so do we need to refetch the video page to get the new token?.
Add support for comments in live youtube videos, api is at: https://www.youtube.com/youtubei/v1/live_chat/get_live_chat?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8.
Make video visible when reading comments (youtube).
Convert nyaa.si/spotify/soundcloud date from ISO date string to local time.
When ui is scaled then the predicated thumbnail size will be wrong since its scaled in Body but not in the plugins where they are requested.
Check if get_page handlers in pages need to check if next batch is valid. If the server returns empty next batch we shouldn't fetch the first page...